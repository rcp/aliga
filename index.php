<?php

require_once('com/db.php');
require_once('com/config.php');

function autoparagraph($par,$num)
{
	if (mb_strlen($par,'UTF-8')>$num)
	{
		$space_place=0;
		for ($i=$num; $i >0 ; $i--) { 
			if (mb_substr($par, $i, 1,'UTF-8')==' ')
			{
				$space_place=$i;
				break;
			}
		}
		if ($space_place>0) $par=mb_substr($par, 0, $space_place,'UTF-8').'</span></li><li><span>'.mb_substr($par, $space_place+1,mb_strlen($par,'UTF-8'));
	}
	return $par;
}


$html_root=$config['html_root'];

$stuff_type=array(
	0=>'inside',
	1=>'video',
	2=>'news');

$db=new database();
$db->connect();

if (isset($_GET['offset']))
{
	$offset=intval($_GET['offset']);
	// echo $offset;
	// die();
}

$categories=array();
$qcat=$db->query('SELECT * FROM ecrm_category WHERE category_parent=0 and category_id>1');
while ($cat=$db->fetch($qcat)) {
  //$prefix='';
  //if (intval($cat['category_type'])==1) $prefix='ИГРА --> ';
  $categories[]=array('id'=>$cat['category_id'],'name'=>$cat['category_name']);
  $qcat2=$db->query('SELECT * FROM ecrm_category WHERE category_parent='.intval($cat['category_id']));
  while ($cat2=$db->fetch($qcat2)) {
    $categories[]=array('id'=>$cat2['category_id'],'name'=>$cat2['category_name']);
  }
}

$qcat1=$db->query('SELECT * FROM ecrm_category WHERE category_parent=0 ORDER BY category_position');
while ($cat1=$db->fetch($qcat1)) {
  $categories_menu[intval($cat1['category_type'])][]=$cat1;
  $cat_names[$cat1['category_id']]=$cat1['category_name'];
}

$qcur=$db->query('SELECT * FROM ecrm_category WHERE category_id=1');
$current=$db->fetch($qcur);
$cfg=json_decode($current['category_config'],true);
/*
{
  "slider1_title": "NFL:",
  "slider1_text": "БРУКЛИН НЕ ОСТАВИЛ ЯНКИС\r\nНИ ОДНОГО ШАНСА НА ВЫХОД В ПЛЕЙ-ОФФ",
  "slider1_link": "test",
  "slider1": "temp/croppedImg_741743590.jpeg",
  "slider2_title": "NFL:",
  "slider2_text": "БРУКЛИН НЕ ОСТАВИЛ ЯНКИС",
  "slider2_link": "test2",
  "slider2": "temp/croppedImg_2143759933.jpeg",
  "slider3_title": "",
  "slider3_text": "",
  "slider3_link": "",
  "slider3": "",
  "slider4_title": "",
  "slider4_text": "",
  "slider4_link": "",
  "slider4": "",
  "slider5_title": "",
  "slider5_text": "",
  "slider5_link": "",
  "slider5": "",
  "number_title": "147 000",
  "number_text": "ЧЕЛОВЕК ПОСЕТИЛИ МАТЧ \r\nПО АМЕРИКАНСКОМУ ФУТБОЛУ \r\nДАЛЛАС - КОЛОРАДО \r\n7 МАРТА 2007 ГОДА",
  "number_link": "test_number",
  "date_title": "20 ФЕВРАЛЯ",
  "date_text": "",
  "date_link": "test_date"
}
*/
//and stuff_status=1
$qnews=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=13 ORDER BY stuff_id DESC LIMIT '.($offset*10).',10');
$news=array();
while ($nnews=$db->fetch($qnews))
{
	$news[]=$nnews;
}

$qitem1=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=1 ORDER BY stuff_id DESC LIMIT '.($offset*5).',5');
$item1=array();
while ($nitem1=$db->fetch($qitem1))
{
	$item1[]=$nitem1;
}

$qitem5=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=5 ORDER BY stuff_id DESC LIMIT '.($offset*3).',3');
$item5=array();
while ($nitem5=$db->fetch($qitem5))
{
	$item5[]=$nitem5;
}

$qitem4=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=4 ORDER BY stuff_id DESC LIMIT '.($offset*6).',6');
$item4=array();
while ($nitem4=$db->fetch($qitem4))
{
	$item4[]=$nitem4;
}

$qitem6=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=6 ORDER BY stuff_id DESC LIMIT '.($offset*2).',2');
$item6=array();
while ($nitem6=$db->fetch($qitem6))
{
	$item6[]=$nitem6;
}

$qitem11=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=11 ORDER BY stuff_id DESC LIMIT '.($offset*9).',9');
$item11=array();
while ($nitem11=$db->fetch($qitem11))
{
	$item11[]=$nitem11;
}

$qitem8=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=8 ORDER BY stuff_id DESC LIMIT '.($offset*1).',1');
$item8=array();
while ($nitem8=$db->fetch($qitem8))
{
	$item8[]=$nitem8;
}

if ($offset!=0)
{
?>
		<div class="row container">
			<div class="col-8">
				<div class="row">
<?

//aut
foreach ($item1 as $key => $value) {
	if ($key<4)
	{
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}

		echo'					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bg-blue text-top">
									<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b clearfix">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
								<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
							</div>
						</div>
					</div>';
	}
	if ($key==1)
	{
		echo '				</div>
				<div class="row mt-25">';
	}

}
?>
				</div>
			</div>
			<div class="col-4">
				<div class="b-h495 bd-5-gray">
					<a href="<?=$cfg['banner_link'];?>"><img src="<?=$cfg['banner'];?>" /></a>
				</div><!-- banner block -->
			</div>
		</div><!-- row container -->
		
		<!--<div class="row container">
			<div class="col-8"> <a href="#"><img src="img/img-b-1.jpg" alt="b1" /></a> </div>
			<div class="col-4"> <a href="#"><img src="img/img-b-2.jpg" alt="b2" /></a> </div>
		</div>-->
		<!-- banners block -->
		
		<div class="row container">
<?
/*
БОЛЬШОЕ ВИДЕО
foreach ($item5 as $key => $value) {
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/0.jpg';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}
echo '			<div class="col-6">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="'.$html_root.'video/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.$value['widget_desc'].'</span></li>
								</ul>
							</a>
						</div>
						<a class="video-b bg-paralax h-300" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'video/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="like-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>
						<div class="view-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>
					</div>
				</div>
			</div>
';
}
*/
?>


		</div><!-- row container -->
		
		<div class="row container">
			<div class="col-4">
				<div class="facts-and-figures-b">
					<div class="i-line-b1"></div>
					<div class="i-line-b2"></div>
					
					<div class="nr-b"><?=$cfg['number_title'];?></div>
					<div class="text-b">
						<?=str_replace("\r\n", '<br />', $cfg['number_text']);?>
					</div>
					<a class="link-b text-underline" href="<?=$cfg['number_link'];?>">ВСЕ ЦИФРЫ  И ФАКТЫ</a>
				</div>
			</div>
			<div class="col-4">
				<div class="main-calendar-b">
					<div class="nr-b">
						<span class="t2"><?=str_replace(' ', '</span>
						<span class="t1">', $cfg['date_title']);?></span>
					</div>
					<div class="text-b">
						<div class="title-b">В ЭТОТ ДЕНЬ</div>
						<div class="inner-b"><?=$cfg['date_text'];?></div>
						<a class="link-b text-underline" href="<?=$cfg['date_link'];?>">ВСЕ ЦИФРЫ  И ФАКТЫ</a>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="main-news-b">
					<div class="title-b clearfix">
						<h3 class="pull-left">НОВОСТИ</h3>
						<div class="tabs-b pull-right">
						</div>
					</div>
					<div class="tabs-content-b">
						<ul class="clearfix" style="display: block">
<?
foreach (array_slice($news, 5, 5) as $key => $value) {

	echo '							<li>
								<div class="img-b"> <b>'.$value['widget_title'].'</b></div>
								<div class="text-b">
									<div class="subject-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_desc'].'</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">'.date('d.m',$value['stuff_date']).'</div>
										<div class="location-b"> <i></i> <span>'.$value['stuff_geo'].'</span></div>
									</div>
								</div>
							</li>';
}
?>
							
						</ul>
					</div>
				<!--<div class="interesting-list-b">
					<div class="title-b">АКТУАЛЬНОЕ</div>
					<ul class="clearfix">
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-7.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> МЕДИА </div>
									<p>
										<span>15:00</span>
										“Cheerleader case” 15 минут славы - и 3 года тренировок
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-8.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b">МАСТЕР-КЛАСС </div>
									<p>
										<span>14:00</span>
										Как встать и творить после 8 пулевых ранений. 50 CENT о жизни и спорте
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-9.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> БЛОГИ </div>
									<p>
										<span>10:00</span>
										Статистика Ле Брон Джеймса в картинках от NBA.COM
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-10.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> ЕДА </div>
									<p>
										<span>09:00</span>
										Удивительный фейхоа рекордсмен среди растений по содержанию йода
									</p>
								</div>
							</a>
						</li>
					</ul>-->
				</div>
			</div>
		</div><!-- row container -->
		
		<div class="container">
			<div class="wr-video-slider-b">
				<ul class="video-slider-b">
<?
foreach ($item4 as $key => $value) {
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type4.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}

echo '					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="'.$html_root.'video/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'video/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<!--<div class="like-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>0</a></span></div>-->
								<div class="view-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</a></span></div>
							</div>
						</div>
					</li>
					';
}
?>
				</ul>
				<div class="video-slider-btn-navig clearfix">
					<div id="video-slider-prev"></div>
					<div id="video-slider-next"></div>
				</div>
			</div><!-- wr-statistic-slider-b -->
		</div><!-- container -->
		<div class="row container">
<?
$value=$item5[0];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
					</div>
				</div>
			</div>
';
?>

<?
$value=$item6[0];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'"><img src="'.$value['widget_img'].'" alt="img" width="194" height="194" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_title'].'</a> </div>
						<p>'.$value['widget_desc'].'</p>
					</div>
				</div>
';
?>
			</div>
<?

$value=$item1[4];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo			
'			<div class="col-4">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
					</div>
				</div>
			</div>';
?>
		</div><!-- row container -->
		
		<div class="bd-12-yellow mt-25">
			<ul class="live-list-b ml-20 clearfix">
<?
for ($i=0;$i<3;$i++)
{
$value=$item11[$i];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
			else $value['widget_img']='/admin/stuff/'.$value['stuff_img'];
		}
		else	
		{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
		}
	echo '				<li>
					<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
					<div class="text-b">
						<div class="subject-b">'.$value['widget_title'].'</div>
						<p>'.$value['widget_desc'].'</p>
					</div>
					<div class="img-b"> <img src="'.$value['widget_img'].'" alt="" /></div>
					</a>
				</li>
';
}

?>

<!--				<li>
					<a href="#">
					<div class="text-b">
						<div class="subject-b">ОБИТЕЛЬ СПОРТА: ИСТОРИЯ ОДНОГО СТАДИОНА </div>
						<p>Седьмая ежегодная премия индустрии  русского сноубординга во всех красках</p>
					</div>
					<div class="img-b"> <img src="img/img-item-circle-3.png" alt="" /></div>
					</a>
				</li>
				<li class="omega">
					<a href="#">
					<div class="text-b">
						<div class="subject-b">КООРДИНАТЫ: АКТИВНЫЙ ОТДЫХ НА БАЛИ</div>
						<p>Скалолазание, bungee-jumping, национальная борьба и традици- онный серфинг во время отпуска</p>
					</div>
					<div class="img-b"><img src="img/img-item-circle-4.png" alt="" /></div>
					</a>
				</li>-->
				
			</ul>
		</div>
		
		<div class="row container">
			<div class="col-8">
				<div class="row">
<?
$value=$item6[1];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'"><img src="'.$value['widget_img'].'" alt="img" width="194" height="194" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_title'].'</a> </div>
						<p>'.$value['widget_desc'].'</p>
					</div>
				</div>
';
?>
					<!--<div class="w-200 ml-10 mr-10 pull-left">
						<div class="main-item-view2">
							<div class="img-b"> <a href="#"><img src="img/img-item-circle-5.png" alt="img" /></a> </div>
							<div class="text-b">
								<div class="subject-b"> <a class="text-underline" href="#">МАСТЕР-КЛАСС</a> </div>
								<p>Правила жизни от Daft Punk и нестареющего Фарелла</p>
							</div>
						</div>
					-->
					</div>
<?
$value=$item5[1];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
					</div>
				</div>
			</div>
';
?>
					<!--<div class="w-416 ml-10 mr-10 pull-left">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">ЛЕГЕНДА:</span></li>
											<li><span>МАЙКЛ ДЖОРДАН: I BELIEVE I CAN FLY</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-250" data-ibg-bg="img/img-item-16.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ЖИЗНЬ</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</div>-->
				</div>

				<div class="row mt-25">
<?
$value=$item5[2];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
					</div>
				</div>
			</div>
';
?><!--
					<div class="w-416 ml-10 mr-10 pull-left">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">ПРОСПОРТ:</span></li>
											<li><span>РЕВОЛЮЦИЯ “4 HOUR BODY” ОТ ТИМОТИ ФЭРРИСА</span></li>
											<li><span>СЖИГАЕТ МИЛЛИОНЫ ПРОБЛЕМ</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-250" data-ibg-bg="img/img-item-17.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ТРЕНИРОВКА</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</div>-->
					<div class="w-200 ml-10 mr-10 pull-left">
<?
$value=$item8[0];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type7.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],18).'</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
							</div>
						</div>
';
?>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="b-h395 bd-5-gray">
					<!--<img src="img/img-b-3.jpg" alt="b2" />-->
				</div>
				
				<div class="fb-like-b bd-4-gray mt-25">
					<!--<img src="img/img-fb-likes.jpg" alt="b3" />-->
					<!--<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Faleagueru&amp;width=306&amp;height=174&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:160px;" allowTransparency="true"></iframe>-->
				</div>
			</div>
		</div><!-- row container -->
		
		<div class="bd-12-yellow mt-25">
			<div class="wr-slide-live-list-b">
				<ul class="slide-live-list-b live-list-b clearfix">
<?
for ($i=3;$i<9;$i++)
{
$value=$item11[$i];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
			else $value['widget_img']='/admin/stuff/'.$value['stuff_img'];
		}
		else	
		{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
		}
	echo '				<li>
					<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
					<div class="text-b">
						<div class="subject-b">'.$value['widget_title'].'</div>
						<p>'.$value['widget_desc'].'</p>
					</div>
					<div class="img-b"> <img src="'.$value['widget_img'].'" alt="" /></div>
					</a>
				</li>
';
}

?>
			<!--		<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">VII-Я МЕЖДУНАРОДНАЯ КОНФЕРЕНЦИЯ YOGA JOURNAL</div>
							<p>Yoga Journal привозит на самое крупное в России событие по йоге уникальных мировых звезд. </p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-6.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ПРЕМИЯ: RUSSIAN  SNOWBOARD AWARDS </div>
							<p>Седьмая ежегодная премия индустрии российского сноубор-динга.</p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-7.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ФЕСТИВАЛЬ: X-MOTION-2013</div>
							<p>Уникальная возможность заняться степом и кросс-фитом под руководством лучших...  </p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-8.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ПРЕМИЯ: RUSSIAN  SNOWBOARD AWARDS </div>
							<p>Седьмая ежегодная премия индустрии российского сноубор-динга.</p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-7.png" alt="" /></div>
						</a>
					</li>-->
				</ul>
				<!--<div class="live-slider-btn-navig clearfix">
					<div id="slide-live-list-prev"></div>
					<div id="slide-live-list-next"></div>
				</div>-->
			</div>
		</div>

<?	
die();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="/favicon.png">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="js/jquery.bxslider.js" type="text/javascript"></script>
	<script src="js/jquery.interactive_bg.js" type="text/javascript"></script>
	<script src="js/myScript.js" type="text/javascript"></script>
<title>А ЛИГА &copy; Журнал о спорте и жизни. NBA, NHL, NFL, MLB, EXTREME, MMA, WORKOUT. // Главная</title>
</head>

<body>
<div class="wrapper-b">
	<div class="header-b relative-b clearfix">
		<a class="logo-b" href="<?=$html_root;?>">a-liga</a>
		<div class="row">
			<div class="col-8">
				<div class="ml-95">
					<div class="quote-b">
            <?
              foreach ($categories_menu[0] as $key => $cat) {
                echo '<span'.(($key>0)?' style="opacity: 0.2;"':'').'>'.$cat['category_name'].'</span> ';
              }
            ?></div>
          <ul class="header-nav-b clearfix">
            <?
              foreach ($categories_menu[1] as $key => $cat) {
              	if ($cat['category_id']!=intval($_GET['s']))
              	{
              		echo '<li><a href="'.$html_root.'sport/'.intval($cat['category_id']).'">'.$cat['category_name'].'</a></li> ';
              	}
                else
                {
                	echo '<li class="active">'.$cat['category_name'].'</li> ';
                }
              }
              ?>
					</ul>
				</div>
			</div>
			<div class="col-4">
				<form class="search-header-b bd-7-yellow pull-right" action="<?=$html_root;?>search/" method="get">
					<input class="pull-right" type="text" name="q" id="" placeholder=""/>
					<input type="submit" value="send" />
				</form>
			</div>
		</div>
		<div class="line-header-gray pull-right"></div>
	</div>
	<div class="content-b">
		<div class="row container pt-35">
			<div class="col-8">
				<div class="wr-main-slider-b">
					<ul class="main-slider-b">
<?
if (strlen($cfg['slider1_link'])>0)
{
	echo '						<li>
							<div class="text-b">
								<a href="'.$cfg['slider1_link'].'">
									<ul>
										<li><span class="name-b text-color-yellow">'.$cfg['slider1_title'].'</span></li>
										<li><span>'.str_replace("\r\n", '</span></li>
										<li><span>', $cfg['slider1_text']).'</span></li>
									</ul>
								</a>
							</div>
							<a href="'.$cfg['slider1_link'].'"><img src="/admin/category/'.$cfg['slider1'].'" alt="img" /></a>
						</li>';
}
?>
<?
if (strlen($cfg['slider2_link'])>0)
{
	echo '						<li>
							<div class="text-b">
								<a href="'.$cfg['slider2_link'].'">
									<ul>
										<li><span class="name-b text-color-yellow">'.$cfg['slider2_title'].'</span></li>
										<li><span>'.str_replace("\r\n", '</span></li>
										<li><span>', $cfg['slider2_text']).'</span></li>
									</ul>
								</a>
							</div>
							<a href="'.$cfg['slider2_link'].'"><img src="/admin/category/'.$cfg['slider2'].'" alt="img" /></a>
						</li>';
}
?>
<?
if (strlen($cfg['slider3_link'])>0)
{
	echo '						<li>
							<div class="text-b">
								<a href="'.$cfg['slider3_link'].'">
									<ul>
										<li><span class="name-b text-color-yellow">'.$cfg['slider3_title'].'</span></li>
										<li><span>'.str_replace("\r\n", '</span></li>
										<li><span>', $cfg['slider3_text']).'</span></li>
									</ul>
								</a>
							</div>
							<a href="'.$cfg['slider3_link'].'"><img src="/admin/category/'.$cfg['slider3'].'" alt="img" /></a>
						</li>';
}
?>

<?
if (strlen($cfg['slider4_link'])>0)
{
	echo '						<li>
							<div class="text-b">
								<a href="'.$cfg['slider4_link'].'">
									<ul>
										<li><span class="name-b text-color-yellow">'.$cfg['slider4_title'].'</span></li>
										<li><span>'.str_replace("\r\n", '</span></li>
										<li><span>', $cfg['slider4_text']).'</span></li>
									</ul>
								</a>
							</div>
							<a href="'.$cfg['slider4_link'].'"><img src="/admin/category/'.$cfg['slider4'].'" alt="img" /></a>
						</li>';
}
?>
<?
if (strlen($cfg['slider5_link'])>0)
{
	echo '						<li>
							<div class="text-b">
								<a href="'.$cfg['slider5_link'].'">
									<ul>
										<li><span class="name-b text-color-yellow">'.$cfg['slider5_title'].'</span></li>
										<li><span>'.str_replace("\r\n", '</span></li>
										<li><span>', $cfg['slider5_text']).'</span></li>
									</ul>
								</a>
							</div>
							<a href="'.$cfg['slider5_link'].'"><img src="/admin/category/'.$cfg['slider5'].'" alt="img" /></a>
						</li>';
}
?>
						
						
					</ul>
					<div class="main-slider-btn-navig clearfix">
						<!--<div id="main-slider-prev"></div>-->
						<div id="main-slider-pager-b"></div>
						<!--<div id="main-slider-next"></div>-->
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="main-news-b">
					<div class="title-b clearfix">
						<h3 class="pull-left">НОВОСТИ</h3>
						<div class="tabs-b pull-right">
							<!--<a class="active" href="tab-1">ВСЕ</a>
							<a href="tab-2">NHL</a>
							<a href="tab-3">NBA</a>
							<a href="tab-4">NFL</a>
							<a href="tab-5">XTREME</a>-->
						</div>
					</div>
					<div class="tabs-content-b">
						<ul id="tabNews-1" class="clearfix" style="display: block">
<?
foreach (array_slice($news, 0, 5) as $key => $value) {

	echo '							<li>
								<div class="img-b"> <b>'.$value['widget_title'].'</b></div>
								<div class="text-b">
									<div class="subject-b"> <a href="'.$html_root.''.$stuff_type[$value['stuff_type']].'/'.$value['stuff_id'].'">'.$value['widget_desc'].'</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">'.date('d.m',$value['stuff_date']).'</div>
										<div class="location-b"> <i></i> <span>'.$value['stuff_geo'].'</span></div>
									</div>
								</div>
							</li>';
}
?>
							
						</ul>
						<ul id="tabNews-2" class="clearfix">
							<li>
								<div class="img-b"> <img src="img/img-2-news.png" alt="news 2" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Назван самый быстрый игрок NFL 2013</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Лос-Анджелес</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-4-news.png" alt="news 4" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Обама устроил чествование чикагским “Медведям”</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Нью-Йорк</span></div>
									</div>
								</div>
							</li>
						</ul>
						<ul id="tabNews-3" class="clearfix">
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-1-news.png" alt="news 1" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Прорайдеры BURTON показывают класс в Альпах</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Куршевель</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-2-news.png" alt="news 2" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Назван самый быстрый игрок NFL 2013</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Лос-Анджелес</span></div>
									</div>
								</div>
							</li>
						</ul>
						<ul id="tabNews-4" class="clearfix">
							<li>
								<div class="img-b"> <img src="img/img-2-news.png" alt="news 2" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Назван самый быстрый игрок NFL 2013</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Лос-Анджелес</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
						</ul>
						<ul id="tabNews-5" class="clearfix">
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
						</ul>
						
					</div>
				</div><!-- main-news-b -->
			</div><!-- col 4 -->
		</div><!-- row -->
		<!--
		<div class="container">
			<div class="wr-statistic-slider-b">
				<ul class="statistic-slider-b">
					<li>
						<div class="img-left">
							<img src="img/ico-team-1.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">100</div>
							<div class="nr-right">99</div>
						</div>
						<div class="img-right">
							<img src="img/ico-team-2.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="img/ico-team-3.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">55</div>
							<div class="nr-right">45</div>
						</div>
						<div class="img-right">
							<img src="img/ico-team-4.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="img/ico-team-5.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">35</div>
							<div class="nr-right">99</div>
						</div>
						<div class="img-right">
							<img src="img/ico-team-6.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="img/ico-team-3.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">112</div>
							<div class="nr-right">87</div>
						</div>
						<div class="img-right">
							<img src="img/ico-team-1.png" alt="img" />
						</div>
					</li>
				</ul>
				<div class="statistic-slider-btn-navig clearfix">
					<div id="statistic-slider-prev"></div>
					<div id="statistic-slider-next"></div>
				</div>
			</div>--><!-- wr-statistic-slider-b -->
		<!--</div>--><!-- container -->
		
		<div class="row container">
			<div class="col-8">
				<div class="row">
<?

//aut
foreach ($item1 as $key => $value) {
	if ($key<4)
	{
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}

		echo'					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bg-blue text-top">
									<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b clearfix">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
								<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
							</div>
						</div>
					</div>';
	}
	if ($key==1)
	{
		echo '				</div>
				<div class="row mt-25">';
	}

}
?>
				</div>
			</div>
			<div class="col-4">
				<div class="b-h495 bd-5-gray">
					<a href="<?=$cfg['banner_link'];?>"><img src="<?=$cfg['banner'];?>" /></a>
				</div><!-- banner block -->
			</div>
		</div><!-- row container -->
		
		<!--<div class="row container">
			<div class="col-8"> <a href="#"><img src="img/img-b-1.jpg" alt="b1" /></a> </div>
			<div class="col-4"> <a href="#"><img src="img/img-b-2.jpg" alt="b2" /></a> </div>
		</div>-->
		<!-- banners block -->
		
		<div class="row container">
<?
/*
БОЛЬШОЕ ВИДЕО
foreach ($item5 as $key => $value) {
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/0.jpg';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}
echo '			<div class="col-6">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="'.$html_root.'video/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.$value['widget_desc'].'</span></li>
								</ul>
							</a>
						</div>
						<a class="video-b bg-paralax h-300" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'video/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="like-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>
						<div class="view-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>
					</div>
				</div>
			</div>
';
}
*/
?>


		</div><!-- row container -->
		
		<div class="row container">
			<div class="col-4">
				<div class="facts-and-figures-b">
					<div class="i-line-b1"></div>
					<div class="i-line-b2"></div>
					
					<div class="nr-b"><?=$cfg['number_title'];?></div>
					<div class="text-b">
						<?=str_replace("\r\n", '<br />', $cfg['number_text']);?>
					</div>
					<a class="link-b text-underline" href="<?=$cfg['number_link'];?>">ВСЕ ЦИФРЫ  И ФАКТЫ</a>
				</div>
			</div>
			<div class="col-4">
				<div class="main-calendar-b">
					<div class="nr-b">
						<span class="t2"><?=str_replace(' ', '</span>
						<span class="t1">', $cfg['date_title']);?></span>
					</div>
					<div class="text-b">
						<div class="title-b">В ЭТОТ ДЕНЬ</div>
						<div class="inner-b"><?=$cfg['date_text'];?></div>
						<a class="link-b text-underline" href="<?=$cfg['date_link'];?>">ВСЕ ЦИФРЫ  И ФАКТЫ</a>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="main-news-b">
					<div class="title-b clearfix">
						<h3 class="pull-left">НОВОСТИ</h3>
						<div class="tabs-b pull-right">
						</div>
					</div>
					<div class="tabs-content-b">
						<ul class="clearfix" style="display: block">
<?
foreach (array_slice($news, 5, 5) as $key => $value) {

	echo '							<li>
								<div class="img-b"> <b>'.$value['widget_title'].'</b></div>
								<div class="text-b">
									<div class="subject-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_desc'].'</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">'.date('d.m',$value['stuff_date']).'</div>
										<div class="location-b"> <i></i> <span>'.$value['stuff_geo'].'</span></div>
									</div>
								</div>
							</li>';
}
?>
							
						</ul>
					</div>
				<!--<div class="interesting-list-b">
					<div class="title-b">АКТУАЛЬНОЕ</div>
					<ul class="clearfix">
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-7.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> МЕДИА </div>
									<p>
										<span>15:00</span>
										“Cheerleader case” 15 минут славы - и 3 года тренировок
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-8.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b">МАСТЕР-КЛАСС </div>
									<p>
										<span>14:00</span>
										Как встать и творить после 8 пулевых ранений. 50 CENT о жизни и спорте
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-9.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> БЛОГИ </div>
									<p>
										<span>10:00</span>
										Статистика Ле Брон Джеймса в картинках от NBA.COM
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-10.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> ЕДА </div>
									<p>
										<span>09:00</span>
										Удивительный фейхоа рекордсмен среди растений по содержанию йода
									</p>
								</div>
							</a>
						</li>
					</ul>-->
				</div>
			</div>
		</div><!-- row container -->
		
		<div class="container">
			<div class="wr-video-slider-b">
				<ul class="video-slider-b">
<?
foreach ($item4 as $key => $value) {
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type4.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}

echo '					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="'.$html_root.'video/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'video/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<!--<div class="like-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>0</a></span></div>-->
								<div class="view-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</a></span></div>
							</div>
						</div>
					</li>
					';
}
?>
				</ul>
				<div class="video-slider-btn-navig clearfix">
					<div id="video-slider-prev"></div>
					<div id="video-slider-next"></div>
				</div>
			</div><!-- wr-statistic-slider-b -->
		</div><!-- container -->
		<div class="row container">
<?
$value=$item5[0];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
					</div>
				</div>
			</div>
';
?>

<?
$value=$item6[0];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}
echo'			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'"><img src="'.$value['widget_img'].'" alt="img" width="194" height="194" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_title'].'</a> </div>
						<p>'.$value['widget_desc'].'</p>
					</div>
				</div>
';
?>
			</div>
<?

$value=$item1[4];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo			
'			<div class="col-4">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
					</div>
				</div>
			</div>';
?>
		</div><!-- row container -->
		
		<div class="bd-12-yellow mt-25">
			<ul class="live-list-b ml-20 clearfix">
<?
for ($i=0;$i<3;$i++)
{
$value=$item11[$i];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
			else $value['widget_img']='/admin/stuff/'.$value['stuff_img'];
		}
		else	
		{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
		}
	echo '				<li>
					<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
					<div class="text-b">
						<div class="subject-b">'.$value['widget_title'].'</div>
						<p>'.$value['widget_desc'].'</p>
					</div>
					<div class="img-b"> <img src="'.$value['widget_img'].'" alt="" /></div>
					</a>
				</li>
';
}

?>

<!--				<li>
					<a href="#">
					<div class="text-b">
						<div class="subject-b">ОБИТЕЛЬ СПОРТА: ИСТОРИЯ ОДНОГО СТАДИОНА </div>
						<p>Седьмая ежегодная премия индустрии  русского сноубординга во всех красках</p>
					</div>
					<div class="img-b"> <img src="img/img-item-circle-3.png" alt="" /></div>
					</a>
				</li>
				<li class="omega">
					<a href="#">
					<div class="text-b">
						<div class="subject-b">КООРДИНАТЫ: АКТИВНЫЙ ОТДЫХ НА БАЛИ</div>
						<p>Скалолазание, bungee-jumping, национальная борьба и традици- онный серфинг во время отпуска</p>
					</div>
					<div class="img-b"><img src="img/img-item-circle-4.png" alt="" /></div>
					</a>
				</li>-->
				
			</ul>
		</div>
		
		<div class="row container">
			<div class="col-8">
				<div class="row">
<?
$value=$item6[1];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}
echo'			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'"><img src="'.$value['widget_img'].'" alt="img" width="194" height="194" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_title'].'</a> </div>
						<p>'.$value['widget_desc'].'</p>
					</div>
				</div>
';
?>
					<!--<div class="w-200 ml-10 mr-10 pull-left">
						<div class="main-item-view2">
							<div class="img-b"> <a href="#"><img src="img/img-item-circle-5.png" alt="img" /></a> </div>
							<div class="text-b">
								<div class="subject-b"> <a class="text-underline" href="#">МАСТЕР-КЛАСС</a> </div>
								<p>Правила жизни от Daft Punk и нестареющего Фарелла</p>
							</div>
						</div>
					-->
					</div>
<?
$value=$item5[1];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
					</div>
				</div>
			</div>
';
?>
					<!--<div class="w-416 ml-10 mr-10 pull-left">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">ЛЕГЕНДА:</span></li>
											<li><span>МАЙКЛ ДЖОРДАН: I BELIEVE I CAN FLY</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-250" data-ibg-bg="img/img-item-16.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ЖИЗНЬ</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</div>-->
				</div>

				<div class="row mt-25">
<?
$value=$item5[2];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
					</div>
				</div>
			</div>
';
?><!--
					<div class="w-416 ml-10 mr-10 pull-left">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">ПРОСПОРТ:</span></li>
											<li><span>РЕВОЛЮЦИЯ “4 HOUR BODY” ОТ ТИМОТИ ФЭРРИСА</span></li>
											<li><span>СЖИГАЕТ МИЛЛИОНЫ ПРОБЛЕМ</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-250" data-ibg-bg="img/img-item-17.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ТРЕНИРОВКА</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</div>-->
					<div class="w-200 ml-10 mr-10 pull-left">
<?
$value=$item8[0];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type7.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='../admin/stuff/'.$value['widget_img'];
	}
echo'						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],18).'</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.$value['stuff_views'].'</span></a></div>
							</div>
						</div>
';
?>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="b-h395 bd-5-gray">
					<!--<img src="img/img-b-3.jpg" alt="b2" />-->
				</div>
				
				<div class="fb-like-b bd-4-gray mt-25">
					<!--<img src="img/img-fb-likes.jpg" alt="b3" />-->
					<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Faleagueru&amp;width=306&amp;height=174&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:160px;" allowTransparency="true"></iframe>
				</div>
			</div>
		</div><!-- row container -->
		
		<div class="bd-12-yellow mt-25">
			<div class="wr-slide-live-list-b">
				<ul class="slide-live-list-b live-list-b clearfix">
<?
for ($i=3;$i<9;$i++)
{
$value=$item11[$i];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
			else $value['widget_img']='/admin/stuff/'.$value['stuff_img'];
		}
		else	
		{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
		}
	echo '				<li>
					<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
					<div class="text-b">
						<div class="subject-b">'.$value['widget_title'].'</div>
						<p>'.$value['widget_desc'].'</p>
					</div>
					<div class="img-b"> <img src="'.$value['widget_img'].'" alt="" /></div>
					</a>
				</li>
';
}

?>
			<!--		<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">VII-Я МЕЖДУНАРОДНАЯ КОНФЕРЕНЦИЯ YOGA JOURNAL</div>
							<p>Yoga Journal привозит на самое крупное в России событие по йоге уникальных мировых звезд. </p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-6.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ПРЕМИЯ: RUSSIAN  SNOWBOARD AWARDS </div>
							<p>Седьмая ежегодная премия индустрии российского сноубор-динга.</p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-7.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ФЕСТИВАЛЬ: X-MOTION-2013</div>
							<p>Уникальная возможность заняться степом и кросс-фитом под руководством лучших...  </p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-8.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ПРЕМИЯ: RUSSIAN  SNOWBOARD AWARDS </div>
							<p>Седьмая ежегодная премия индустрии российского сноубор-динга.</p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-7.png" alt="" /></div>
						</a>
					</li>-->
				</ul>
				<div class="live-slider-btn-navig clearfix">
					<div id="slide-live-list-prev"></div>
					<div id="slide-live-list-next"></div>
				</div>
			</div>
		</div>
		<?
// echo '1231321321';
		?>

		<a href="#" class="show-more">
		<div class="bd-12-gray mt-25 showmore">
			<img src="/img/more.png" alt="Показать еще" title="Показать еще"/>
		</div>
		</a>


	</div><!-- content -->
	
	<div class="footer-b container clearfix">
        <a href="" class="logo-g"><img src="img/logo-gray.png" alt="logo" /></a>
        <div class="copyright-b">
            <span>&copy; 2014. А ЛИГА.</span>
            <p>Интернет-издание об игровых <br />видах спорта, активном образе <br />жизни и не только.</p>
        </div>

        <div class="text-b">
        	<div class="footer-b__social pull-right">
        		<p class="pull-left ttl-b"> <span>FOLLOW US</span> <img src="img/ico-a.png" alt="" /></p>
        		<ul class="pull-right">
        			<li><a class="ico-b i-tw" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-y" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-gp" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-vk" href="http://vk.com/a.league" target="_blank"></a></li>
        			<li><a class="ico-b i-fb" href="http://facebook.com/aleagueru" target="_blank"></a></li>
        			<li><a class="ico-b i-pr" href="http://instagram.com/a.league" target="_blank"></a></li>
        		</ul>
        	</div>
            
            <ul class="footer-b__nav pull-right">
                <li><a href="/inside/530">ПОМОЩЬ</a></li>
                <li><a href="/inside/529">КОНТАКТЫ</a></li>
                <li><a href="/inside/532">ПРАВА</a></li>
                <li><a href="/inside/533">РЕКЛАМОДАТЕЛЯМ</a></li>
            </ul>
        </div>
	</div><!-- content -->
</div><!-- wrapper-b -->
<script type="text/javascript">
var show_offset=0;
$('.show-more').click(function() {
	show_offset++;
	$.ajax({
  url: "?offset="+show_offset
})
  .done(function( data ) {
  	$('.show-more').before(''+data+'');
  });
  return false;
});
</script>
</body>
</html>
