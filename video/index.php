<?php

require_once('../com/db.php');
require_once('../com/config.php');

$html_root=$config['html_root'];

$db=new database();
$db->connect();

$qviews=$db->query('UPDATE ecrm_stuff SET stuff_views=stuff_views+1 WHERE stuff_id='.intval($_GET['s']).' LIMIT 1');
$views=$db->fetch($qviews);


$qnews=$db->query('SELECT * FROM ecrm_stuff WHERE stuff_id='.intval($_GET['s']).' LIMIT 1');
$new=$db->fetch($qnews);

if (strlen($new['stuff_head_title'])==0) $new['stuff_head_title']='Журнал о спорте и жизни. NBA, NHL, NFL, MLB, EXTREME, MMA, WORKOUT.';

if (strlen($new['stuff_img'])>0)
{
  $title_img='/'.$new['stuff_img'];
}
else
{
  $title_img='/admin/img_uploads/0.jpg';
}

$categories=array();
$qcat=$db->query('SELECT * FROM ecrm_category WHERE category_parent=0 and category_id>1');
while ($cat=$db->fetch($qcat)) {
  //$prefix='';
  //if (intval($cat['category_type'])==1) $prefix='ИГРА --> ';
  if (intval($cat['category_id'])==intval($new['category_id']))
  {
  	$new['category_name']=$cat['category_name'];
  }
  $categories[]=array('id'=>$cat['category_id'],'name'=>$cat['category_name']);
  $qcat2=$db->query('SELECT * FROM ecrm_category WHERE category_parent='.intval($cat['category_id']));
  while ($cat2=$db->fetch($qcat2)) {
    $categories[]=array('id'=>$cat2['category_id'],'name'=>$cat2['category_name']);
  }
}

$qcat1=$db->query('SELECT * FROM ecrm_category WHERE category_parent=0 ORDER BY category_position');
while ($cat1=$db->fetch($qcat1)) {
  $categories_menu[intval($cat1['category_type'])][]=$cat1;
  $cat_names[$cat1['category_id']]=$cat1['category_name'];
}

$qrub=$db->query('SELECT * FROM aliga_rubrika ORDER BY rubrika_position');
while ($nrub=$db->fetch($qrub)) {
  $rubrika[$nrub['rubrika_id']]=$nrub['rubrika_name'];
}

$qcur=$db->query('SELECT * FROM ecrm_category WHERE category_id='.intval($new['category_id']));
$current=$db->fetch($qcur);
$cfg=json_decode($current['category_config'],true);

function autoparagraph($par,$num)
{
	if (mb_strlen($par,'UTF-8')>$num)
	{
		$space_place=0;
		for ($i=$num; $i >0 ; $i--) { 
			if (mb_substr($par, $i, 1,'UTF-8')==' ')
			{
				$space_place=$i;
				break;
			}
		}
		if ($space_place>0) $par=mb_substr($par, 0, $space_place,'UTF-8').'</span></li><li><span>'.mb_substr($par, $space_place+1,mb_strlen($par,'UTF-8'));
	}
	return $par;
}

$tgs=explode(',',$new['stuff_tags']);
$search_query='';
$sep='';
foreach ($tgs as $key => $tgss) {
		$tgss=mb_strtolower($tgss,'utf-8');
		$search_query.=$sep.'LOWER(stuff_tags) LIKE "'.addslashes($tgss).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($tgss).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($tgss).',%" OR LOWER(stuff_tags) = "'.addslashes($tgss).'"';
		$sep=' OR ';
}
if (mb_strlen($search_query)>2)
{
	$qitem1=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=1 and ('.$search_query.') ORDER BY stuff_id DESC LIMIT 7');
	$item1=array();
	while ($nitem1=$db->fetch($qitem1))
	{
		$item1[]=$nitem1;
		$total_count++;
	}

	$qitem5=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=5 and ('.$search_query.') ORDER BY stuff_id DESC LIMIT 2');
	$item5=array();
	while ($nitem5=$db->fetch($qitem5))
	{
		$item5[]=$nitem5;
		$total_count++;
	}

	$qitem6=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=6 and ('.$search_query.') ORDER BY stuff_id DESC LIMIT 2');
	$item6=array();
	while ($nitem6=$db->fetch($qitem6))
	{
		$item6[]=$nitem6;
		$total_count++;
	}

	$qitem4=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=4 and ('.$search_query.') ORDER BY stuff_id DESC LIMIT 6');
	$item4=array();
	while ($nitem4=$db->fetch($qitem4))
	{
		$item4[]=$nitem4;
		$total_count++;
	}

	$qitem11=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=11 and'.$search_query.')  ORDER BY stuff_id DESC LIMIT 3');
	$item11=array();
	while ($nitem11=$db->fetch($qitem11))
	{
		$item11[]=$nitem11;
		$total_count++;
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="/favicon.png">
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="../js/jquery.bxslider.js" type="text/javascript"></script>
	<script src="../js/jquery.interactive_bg.js" type="text/javascript"></script>
	<script src="../js/myScript.js" type="text/javascript"></script>
<title>А ЛИГА &copy; <?=$new['stuff_head_title'];?></title>
</head>

<body>
<div class="wrapper-b">
	<div class="header-b relative-b clearfix">
		<a class="logo-b" href="<?=$html_root;?>">a-liga</a>
		<div class="row">
			<div class="col-8">
				<div class="ml-95">
					<div class="quote-b">            <?
              foreach ($categories_menu[0] as $key => $cat) {
                echo '<span'.(($key>0)?' style="opacity: 0.2;"':'').'>'.$cat['category_name'].'</span> ';
              }
            ?></div>
          <ul class="header-nav-b clearfix">
            <?
              foreach ($categories_menu[1] as $key => $cat) {
                echo '<li><a href="'.$html_root.'sport/'.intval($cat['category_id']).'">'.$cat['category_name'].'</a></li> ';
              }
              ?>
					</ul>
				</div>
			</div>
			<div class="col-4">
				<form class="search-header-b bd-7-yellow pull-right" action="<?=$html_root;?>search/" method="get">
					<input class="pull-right" type="text" name="q" id="" placeholder=""/>
					<input type="submit" value="send" />
				</form>
			</div>
		</div>
		<div class="line-header-gray pull-right"></div>
	</div><!-- header-b -->
	
	<div class="content-b">
		
		<div class="row container">
			<div class="col-8">
				<div class="main-video-b">
					<div class="name-b"><?=(strlen($new['stuff_title'])>0?$new['stuff_title']:'Заголовок');?></div>
					<?=(strlen($new['stuff_content'])>0?str_replace('//www.youtube.com/watch?v=', '//www.youtube.com/embed/', $new['stuff_content']):'<iframe width="636" height="360" src="http://www.youtube.com/embed/2JKeWGz-ZxQ" frameborder="0" allowfullscreen></iframe>');?>
					<div class="params-b clearfix">
						<div class="type-b"> <a href="<?=$html_root;?>sport/<?=$new['category_id'];?>"><?=$new['category_name'];?></a></div>
						<?
						if (intval($new['rubrika_id'])>0)
						{
						?>
						<div class="rubrika-b"> <?=$rubrika[$new['rubrika_id']];?> </div>
						<?
						}
						?>
						<!--<div class="like-b"><a href="#"><i></i> <span>0</a></span></div>-->
						<div class="view-b"><a href="#"><i></i> <span><?=$new['stuff_views'];?></a></span></div>
						<div class="date-b"><a href="#"><?=date('d.m',$new['stuff_date']);?></a></div>
					</div>
				</div>
				<div class="bd-10-yellow pt-15 pb-15 mt-2 clearfix">
					<ul class="main-social-b ml-25 pull-left">
						<li class="i-fb"><a href="#">fb</a></li>
						<li class="i-vk"><a href="#">vk</a></li>
						<li class="i-twitter"><a href="#">twitter</a></li>
						<li class="i-gp"><a href="#">gp</a></li>
						<li class="i-info"><a href="#">info</a></li>
					</ul>
					<div class="counter-comment-b pull-right mr-45">
						<a class="lh-50" href="#disqus_thread">КОММЕНТАРИИ ( 0 )</a>
					</div>
				</div>
				
				<div class="tags-b ml-40 mt-25 clearfix">
					<i></i>
					<div class="pull-left ml-50">
<?
		$tags=explode(',', $new['stuff_tags']);
		foreach ($tags as $key => $value) {
						if ((($key%4)==0)&&($key>0)) echo '<br/><br/>';
			//echo '<a href="/new/?tag='.urlencode($value).'">'.$value.'</a>';
			echo '<a href="'.$html_root.'search/?tag='.urlencode($value).'">'.$value.'</a>';
		}
		
?>
					</div>
				</div>
				
    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
        var disqus_shortname = 'aliga'; // required: replace example with your forum shortname

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
    

				<div class="title-type1 ml-40 mt-25 clearfix">
					<i></i>
					<h3 class="ml-50">ВАС ТАКЖЕ МОЖЕТ ЗАИНТЕРЕСОВАТЬ: </h3>
				</div>
			</div>
			<div class="col-4">
				<div class="h-45 bd-4-gray"></div>
				<div class="b-h496 bd-4-gray_wo_border mt-25">
					<a href="<?=$cfg['banner_link'];?>"><img src="/admin/category/<?=$cfg['banner'];?>" width="308" height="505" alt="img" /></a>
				</div>
			</div>
		</div><!-- article-block -->

























	
		<div class="container" <?=(($total_count==0)|(count($item1)==0&&count($item4)==0)?'style=" visibility: hidden; height: 0px;"':'');?>>
			<div class="wr-video-slider-b">
				<ul class="video-slider-b">
<?

foreach ($item1 as $key => $value) {
	if ($key<6)
	{
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
		echo'					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bg-blue text-top">
									<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
								<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.intval($value['stuff_views']).'</span></a></div>
							</div>
						</div>
					</div>';
	}
}

foreach ($item4 as $key => $value) {
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type4.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}

echo '					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="'.$html_root.'video/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'video/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<!--<div class="like-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>0</a></span></div>-->
								<div class="view-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>'.intval($value['stuff_views']).'</a></span></div>
							</div>
						</div>
					</li>
					';
}
?>

				</ul>
				<div class="video-slider-btn-navig clearfix">
					<div id="video-slider-prev"></div>
					<div id="video-slider-next"></div>
				</div>
			</div><!-- wr-statistic-slider-b -->
		</div><!-- container -->
		
		<div class="row container" <?=($total_count==0|(count($item6)==0&&count($item1)<7&&count($item5)==0)?' style="visibility: hidden; height: 0px;"':'');?>>
<?

$value=$item6[0];
//print_r($value);
//die();
error_reporting(LC_ALL);
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'"><img src="'.$value['widget_img'].'" alt="img" width="194" height="194" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_title'].'</a> </div>
						<p>'.$value['widget_desc'].'</p>
					</div>
				</div>

';
?>
</div>
			
<?
$value=$item5[1];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.intval($value['stuff_views']).'</span></a></div>
					</div>
				</div>
			</div>
';

?>

			
<?

$value=$item1[6];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo			
'			<div class="col-4">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.intval($value['stuff_views']).'</span></a></div>
					</div>
				</div>
			</div>';
?>

		</div><!-- row container -->
		




































	</div><!-- content -->
	
	<div class="footer-b container clearfix">
        <a href="" class="logo-g"><img src="../img/logo-gray.png" alt="logo" /></a>
        <div class="copyright-b">
            <span>&copy; 2014. А ЛИГА.</span>
            <p>Интернет-издание об игровых <br />видах спорта, активном образе <br />жизни и не только.</p>
        </div>

        <div class="text-b">
        	<div class="footer-b__social pull-right">
        		<p class="pull-left ttl-b"> <span>FOLLOW US</span> <img src="../img/ico-a.png" alt="" /></p>
        		<ul class="pull-right">
        			<li><a class="ico-b i-tw" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-y" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-gp" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-vk" href="http://vk.com/a.league" target="_blank"></a></li>
        			<li><a class="ico-b i-fb" href="http://facebook.com/aleagueru" target="_blank"></a></li>
        			<li><a class="ico-b i-pr" href="http://instagram.com/a.league" target="_blank"></a></li>
        		</ul>
        	</div>
            
            <ul class="footer-b__nav pull-right">
                <li><a href="/inside/530">ПОМОЩЬ</a></li>
                <li><a href="/inside/529">КОНТАКТЫ</a></li>
                <li><a href="/inside/532">ПРАВА</a></li>
                <li><a href="/inside/533">РЕКЛАМОДАТЕЛЯМ</a></li>
            </ul>
        </div>
	</div><!-- content -->
</div><!-- wrapper-b -->
    <script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'aliga'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function () {
        var s = document.createElement('script'); s.async = true;
        s.type = 'text/javascript';
        s.src = '//' + disqus_shortname + '.disqus.com/count.js';
        (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
    }());
    </script>

</body>
</html>