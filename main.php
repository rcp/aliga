<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="js/jquery.bxslider.js" type="text/javascript"></script>
	<script src="js/myScript.js" type="text/javascript"></script>
<title>Aleague &copy;</title>
</head>

<body>
<div class="wrapper-b">
	<div class="header-b relative-b clearfix">
		<a class="logo-b" href="#">a-liga</a>
		<div class="row">
			<div class="col-8">
				<div class="ml-95">
					<div class="quote-b"> <span>ИГРА</span> <span>ЖИЗНЬ </span> <span>ТРЕНИРОВКА </span> <span>ТАЙМ-АУТ </span> </div>
					<ul class="header-nav-b clearfix">
						<li><a href="#">ХОККЕЙ</a></li>
						<li><a href="#">БАСКЕТБОЛ</a></li>
						<li><a href="#">АМЕРИКАНСКИЙ ФУТБОЛ</a></li>
						<li><a href="#">БЕЙСБОЛ</a></li>
						<li><a href="#">XTREME</a></li>
						<li><a href="#">БОЕВЫЕ</a></li>
						<li class="pr-0"><a href="#">ЕЩЕ</a></li>
					</ul>
				</div>
			</div>
			<div class="col-4">
				<form class="search-header-b bd-7-yellow pull-right" action="">
					<input class="pull-right" type="text" name="" id="" placeholder=""/>
					<input type="submit" value="send" />
				</form>
			</div>
		</div>
		<div class="line-header-gray pull-right"></div>
	</div>
	<div class="content-b">
		<div class="row container">
			<div class="col-8">
				<div class="wr-main-slider-b">
					<ul class="main-slider-b">
						<li>
							<div class="text-b">
								<a href="#">
									<ul>
										<li><span class="name-b">NFL:</span></li>
										<li><span>БРУКЛИН НЕ ОСТАВИЛ ЯНКИС</span></li>
										<li><span>НИ ОДНОГО ШАНСА НА ВЫХОД В ПЛЕЙ-ОФФ</span></li>
									</ul>
								</a>
							</div>
							<img src="img/img-slider-main.jpg" alt="img" />
						</li>
						<li>
							<div class="text-b">
								<a href="#">
									<ul>
										<li><span class="name-b">NFL:</span></li>
										<li><span>БРУКЛИН НЕ ОСТАВИЛ ЯНКИС</span></li>
										<li><span>НИ ОДНОГО ШАНСА НА ВЫХОД В ПЛЕЙ-ОФФ</span></li>
									</ul>
								</a>
							</div>
							<img src="img/img-slider-main2.jpg" alt="img" />
						</li>
						<li>
							<div class="text-b">
								<a href="#">
									<ul>
										<li><span class="name-b">NFL:</span></li>
										<li><span>БРУКЛИН НЕ ОСТАВИЛ ЯНКИС</span></li>
										<li><span>НИ ОДНОГО ШАНСА НА ВЫХОД В ПЛЕЙ-ОФФ</span></li>
									</ul>
								</a>
							</div>
							<img src="img/img-slider-main3.jpg" alt="img" />
						</li>
						<li>
							<div class="text-b">
								<a href="#">
									<ul>
										<li><span class="name-b">NFL:</span></li>
										<li><span>БРУКЛИН НЕ ОСТАВИЛ ЯНКИС</span></li>
										<li><span>НИ ОДНОГО ШАНСА НА ВЫХОД В ПЛЕЙ-ОФФ</span></li>
									</ul>
								</a>
							</div>
							<img src="img/img-slider-main.jpg" alt="img" />
						</li>
						<li>
							<div class="text-b">
								<a href="#">
									<ul>
										<li><span class="name-b">NFL:</span></li>
										<li><span>БРУКЛИН НЕ ОСТАВИЛ ЯНКИС</span></li>
										<li><span>НИ ОДНОГО ШАНСА НА ВЫХОД В ПЛЕЙ-ОФФ</span></li>
									</ul>
								</a>
							</div>
							<img src="img/img-slider-main.jpg" alt="img" />
						</li>
						
						
					</ul>
					<div class="main-slider-btn-navig clearfix">
						<div id="main-slider-prev"></div>
						<div id="main-slider-pager-b"></div>
						<div id="main-slider-next"></div>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="main-news-b">
					<div class="title-b clearfix">
						<h3 class="pull-left">НОВОСТИ</h3>
						<div class="tabs-b pull-right">
							<a class="active" href="tab-1">ВСЕ</a>
							<a href="tab-2">NHL</a>
							<a href="tab-3">NBA</a>
							<a href="tab-4">NFL</a>
							<a href="tab-5">XTREME</a>
						</div>
					</div>
					<div class="tabs-content-b">
						<ul id="tabNews-1" class="clearfix" style="display: block">
							<li>
								<div class="img-b"> <img src="img/img-1-news.png" alt="news 1" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Прорайдеры BURTON показывают класс в Альпах</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Куршевель</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-2-news.png" alt="news 2" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Назван самый быстрый игрок NFL 2013</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Лос-Анджелес</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-4-news.png" alt="news 4" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Обама устроил чествование чикагским “Медведям”</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Нью-Йорк</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-5-news.png" alt="news 5" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">“Манчестер Юнайтед” обогнал “Нью-Йорк Янкис” в списке самых дорогих спортив- ных брендов мира</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Манчестер</span></div>
									</div>
								</div>
							</li>
							
						</ul>
						<ul id="tabNews-2" class="clearfix">
							<li>
								<div class="img-b"> <img src="img/img-2-news.png" alt="news 2" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Назван самый быстрый игрок NFL 2013</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Лос-Анджелес</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-4-news.png" alt="news 4" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Обама устроил чествование чикагским “Медведям”</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Нью-Йорк</span></div>
									</div>
								</div>
							</li>
						</ul>
						<ul id="tabNews-3" class="clearfix">
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-1-news.png" alt="news 1" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Прорайдеры BURTON показывают класс в Альпах</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Куршевель</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-2-news.png" alt="news 2" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Назван самый быстрый игрок NFL 2013</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Лос-Анджелес</span></div>
									</div>
								</div>
							</li>
						</ul>
						<ul id="tabNews-4" class="clearfix">
							<li>
								<div class="img-b"> <img src="img/img-2-news.png" alt="news 2" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Назван самый быстрый игрок NFL 2013</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Лос-Анджелес</span></div>
									</div>
								</div>
							</li>
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
						</ul>
						<ul id="tabNews-5" class="clearfix">
							<li>
								<div class="img-b"> <img src="img/img-3-news.png" alt="news 3" /></div>
								<div class="text-b">
									<div class="subject-b"> <a href="#">Битва титанов на льду в Торонто</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">15.11</div>
										<div class="location-b"> <i></i> <span>Торонто</span></div>
									</div>
								</div>
							</li>
						</ul>
						
					</div>
				</div><!-- main-news-b -->
			</div><!-- col 4 -->
		</div><!-- row -->
		
		<div class="container">
			<div class="wr-statistic-slider-b">
				<ul class="statistic-slider-b">
					<li>
						<div class="img-left">
							<img src="img/ico-team-1.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">100</div>
							<div class="nr-right">99</div>
						</div>
						<div class="img-right">
							<img src="img/ico-team-2.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="img/ico-team-3.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">55</div>
							<div class="nr-right">45</div>
						</div>
						<div class="img-right">
							<img src="img/ico-team-4.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="img/ico-team-5.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">35</div>
							<div class="nr-right">99</div>
						</div>
						<div class="img-right">
							<img src="img/ico-team-6.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="img/ico-team-3.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">112</div>
							<div class="nr-right">87</div>
						</div>
						<div class="img-right">
							<img src="img/ico-team-1.png" alt="img" />
						</div>
					</li>
				</ul>
				<div class="statistic-slider-btn-navig clearfix">
					<div id="statistic-slider-prev"></div>
					<div id="statistic-slider-next"></div>
				</div>
			</div><!-- wr-statistic-slider-b -->
		</div><!-- container -->
		
		<div class="row container">
			<div class="col-8">
				<div class="row">
					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bg-blue text-top">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">XTREME:</span></li>
											<li><span>ГЛАЗА БОЯТСЯ, РУКИ</span></li>
											<li><span>ДЕЛАЮТ</span></li>
										</ul>
									</a>
								</div>
								<a href="#"><img src="img/img-item-1.jpg" alt="img" /></a>
							</div>
							<div class="params-b clearfix">
								<div class="name-b"><a href="#">ТРЕНИРОВКА</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>1013</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>12672</span></a></div>
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bg-green text-top">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">MMA:</span></li>
											<li><span>147 ПОБЕД МЭННИ</span></li>
											<li><span>ПАКЬЯО</span></li>
										</ul>
									</a>
								</div>
								<a href="#"><img src="img/img-item-2.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ЗАЛ СЛАВЫ</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>1013</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>12672</span></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row mt-25">
					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bg-blue text-top">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">ХОККЕЙ:</span></li>
											<li><span>КАК НАШИ КУБОК</span></li>
											<li><span>СВИСТНУЛИ</span></li>
										</ul>
									</a>
								</div>
								<a href="#"><img src="img/img-item-3.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ЗАЛ СЛАВЫ</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>1013</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>12672</span></a></div>
							</div>
						</div>
					</div>
					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-top">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">NASCAR:</span></li>
											<li><span>ШВЕДЫ ВЫЛЕТЕЛИ</span></li>
											<li><span>В ТРУБУ НА ЧМ 2013</span></li>
										</ul>
									</a>
								</div>
								<a href="#"><img src="img/img-item-4.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">НОВОСТИ</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>1013</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>12672</span></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="b-h495 bd-5-gray">
					
				</div><!-- banner block -->
			</div>
		</div><!-- row container -->
		
		<div class="row container">
			<div class="col-8"> <a href="#"><img src="img/img-b-1.jpg" alt="b1" /></a> </div>
			<div class="col-4"> <a href="#"><img src="img/img-b-2.jpg" alt="b2" /></a> </div>
		</div><!-- banners block -->
		
		<div class="row container">
			<div class="col-6">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="#">
								<ul>
									<li><span class="text-underline text-color-yellow">ФОТО ДНЯ:</span></li>
									<li><span>СТОЯТ ДЕВЧОНКИ, СТОЯТ В СТОРОНКЕ</span></li>
								</ul>
							</a>
						</div>
						<a href="#"><img src="img/img-item-5.jpg" alt="img" /></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="#">ТРЕНИРОВКА</a></div>
						<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
						<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
						<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="#">
								<ul>
									<li><span class="text-underline text-color-yellow">ВИДЕО ДНЯ:</span></li>
									<li><span>КОСМИЧЕСКИЙ ШОУ-ДРИББЛИНГ</span></li>
								</ul>
							</a>
						</div>
						<a class="video-b" href="#"><img src="img/img-item-6.jpg" alt="img" /></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="#">ВИДЕО</a></div>
						<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
						<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
						<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
					</div>
				</div>
			</div>
		</div><!-- row container -->
		
		<div class="row container">
			<div class="col-4">
				<div class="facts-and-figures-b">
					<div class="i-line-b1"></div>
					<div class="i-line-b2"></div>
					
					<div class="nr-b">147 000</div>
					<div class="text-b">
						ЧЕЛОВЕК ПОСЕТИЛИ МАТЧ <br />
						ПО АМЕРИКАНСКОМУ ФУТБОЛУ <br />
						 ДАЛЛАС - КОЛОРАДО <br />
						7 МАРТА 2007 ГОДА
					</div>
					<a class="link-b text-underline" href="#">ВСЕ ЦИФРЫ  И ФАКТЫ</a>
				</div>
			</div>
			<div class="col-4">
				<div class="main-calendar-b">
					<div class="nr-b">
						<span class="t2">20</span>
						<span class="t1">ФЕВРАЛЯ</span>
					</div>
					<div class="text-b">
						<div class="title-b">В ЭТОТ ДЕНЬ</div>
						<div class="inner-b"></div>
						<a class="link-b text-underline" href="#">ВСЕ ЦИФРЫ  И ФАКТЫ</a>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="interesting-list-b">
					<div class="title-b">АКТУАЛЬНОЕ</div>
					<ul class="clearfix">
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-7.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> МЕДИА </div>
									<p>
										<span>15:00</span>
										“Cheerleader case” 15 минут славы - и 3 года тренировок
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-8.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b">МАСТЕР-КЛАСС </div>
									<p>
										<span>14:00</span>
										Как встать и творить после 8 пулевых ранений. 50 CENT о жизни и спорте
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-9.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> БЛОГИ </div>
									<p>
										<span>10:00</span>
										Статистика Ле Брон Джеймса в картинках от NBA.COM
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="#">
								<div class="img-b"> <img src="img/img-item-10.jpg" alt="img" /></div>
								<div class="text-b">
									<div class="subject-b"> ЕДА </div>
									<p>
										<span>09:00</span>
										Удивительный фейхоа рекордсмен среди растений по содержанию йода
									</p>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div><!-- row container -->
		
		<div class="container">
			<div class="wr-video-slider-b">
				<ul class="video-slider-b">
					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="#">
										<ul>
											<li><span class="text-underline">БЕЙСБОЛ:</span></li>
											<li><span>НЕОЖИДАННАЯ КОНЦОВКА МАТЧА</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b" href="#"><img src="img/img-item-11.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ВИДЕО</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</a></span></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</a></span></div>
							</div>
						</div>
					</li>
					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="#">
										<ul>
											<li><span class="text-underline">XTREME:</span></li>
											<li><span>ДАЕШЬ ОТКРОВЕННЫЙ СЕРФИНГ</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b" href="#"><img src="img/img-item-12.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ВИДЕО</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</li>
					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline">РЕКЛАМА:</span></li>
											<li><span>ASKET JINGLE ALL THE WAY !</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b" href="#"><img src="img/img-item-13.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ВИДЕО</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</li>
				</ul>
				<div class="video-slider-btn-navig clearfix">
					<div id="video-slider-prev"></div>
					<div id="video-slider-next"></div>
				</div>
			</div><!-- wr-statistic-slider-b -->
		</div><!-- container -->
		
		<div class="row container">
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="#">
								<ul>
									<li><span class="text-underline text-color-yellow">КИНО:</span></li>
									<li><span>ЧЕЛОВЕК, КОТОРЫЙ ИЗМЕНИЛ ВСЕ</span></li>
								</ul>
							</a>
						</div>
						<a class="video-b" href="#"><img src="img/img-item-14.jpg" alt="img" /></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="#">ВИДЕО</a></div>
						<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
						<div class="date-b"><a href="#">KINOPOISK 7,8</a></div>
						<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
						<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
					</div>
				</div>
			</div>
			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="#"><img src="img/img-item-circle-1.png" alt="img" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="#">НАСТРОЙ</a> </div>
						<p>Первородная мотивация от секс символов современности</p>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="#">
								<ul>
									<li><span class="text-underline text-color-yellow">ПО МЕСТАМ:</span></li>
									<li><span>ИСТОРИЯ ВЕЛИКОГО СТАДИОНА</span></li>
									<li><span>СЕНТ-ЛУИС</span></li>
								</ul>
							</a>
						</div>
						<a class="video-b" href="#"><img src="img/img-item-15.jpg" alt="img" /></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="#">ТАЙМ - АУТ</a></div>
						<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
						<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
						<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
					</div>
				</div>
			</div>
		</div><!-- row container -->
		
		<div class="bd-12-yellow mt-25">
			<ul class="live-list-b ml-20 clearfix">
				<li>
					<a href="#">
					<div class="text-b">
						<div class="subject-b">ПО ДОМАМ: ИНТЕРЬЕРЫ ОЛИМПА</div>
						<p>Подборка 10 лучших домов, приспособленных для жизни разнообразных чемпионов</p>
					</div>
					<div class="img-b"> <img src="img/img-item-circle-2.png" alt="" /></div>
					</a>
				</li>
				<li>
					<a href="#">
					<div class="text-b">
						<div class="subject-b">ОБИТЕЛЬ СПОРТА: ИСТОРИЯ ОДНОГО СТАДИОНА </div>
						<p>Седьмая ежегодная премия индустрии  русского сноубординга во всех красках</p>
					</div>
					<div class="img-b"> <img src="img/img-item-circle-3.png" alt="" /></div>
					</a>
				</li>
				<li class="omega">
					<a href="#">
					<div class="text-b">
						<div class="subject-b">КООРДИНАТЫ: АКТИВНЫЙ ОТДЫХ НА БАЛИ</div>
						<p>Скалолазание, bungee-jumping, национальная борьба и традици- онный серфинг во время отпуска</p>
					</div>
					<div class="img-b"><img src="img/img-item-circle-4.png" alt="" /></div>
					</a>
				</li>
				
			</ul>
		</div>
		
		<div class="row container">
			<div class="col-8">
				<div class="row">
					<div class="w-200 ml-10 mr-10 pull-left">
						<div class="main-item-view2">
							<div class="img-b"> <a href="#"><img src="img/img-item-circle-5.png" alt="img" /></a> </div>
							<div class="text-b">
								<div class="subject-b"> <a class="text-underline" href="#">МАСТЕР-КЛАСС</a> </div>
								<p>Правила жизни от Daft Punk и нестареющего Фарелла</p>
							</div>
						</div>
					</div>
					<div class="w-416 ml-10 mr-10 pull-left">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">ЛЕГЕНДА:</span></li>
											<li><span>МАЙКЛ ДЖОРДАН: I BELIEVE I CAN FLY</span></li>
										</ul>
									</a>
								</div>
								<a href="#"><img src="img/img-item-16.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ЖИЗНЬ</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row mt-25">
					<div class="w-416 ml-10 mr-10 pull-left">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">ПРОСПОРТ:</span></li>
											<li><span>РЕВОЛЮЦИЯ “4 HOUR BODY” ОТ ТИМОТИ ФЭРРИСА</span></li>
											<li><span>СЖИГАЕТ МИЛЛИОНЫ ПРОБЛЕМ</span></li>
										</ul>
									</a>
								</div>
								<a href="#"><img src="img/img-item-17.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ТРЕНИРОВКА</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</div>
					<div class="w-200 ml-10 mr-10 pull-left">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline text-color-yellow">МЕДИА:</span></li>
											<li><span>ЛЕГЕНДЫ БЕЙСБОЛА</span></li>
											<li><span>НА ЗАСТАВКУ</span></li>
										</ul>
									</a>
								</div>
								<a href="#"><img src="img/img-item-18.jpg" alt="img" /></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ТАЙМ - АУТ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="b-h395 bd-5-gray">
					<img src="img/img-b-3.jpg" alt="b2" />
				</div>
				
				<div class="fb-like-b bd-4-gray mt-25">
					<img src="img/img-fb-likes.jpg" alt="b3" />
				</div>
			</div>
		</div><!-- row container -->
		
		<div class="bd-12-yellow mt-25">
			<div class="wr-slide-live-list-b">
				<ul class="slide-live-list-b live-list-b clearfix">
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">VII-Я МЕЖДУНАРОДНАЯ КОНФЕРЕНЦИЯ YOGA JOURNAL</div>
							<p>Yoga Journal привозит на самое крупное в России событие по йоге уникальных мировых звезд. </p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-6.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ПРЕМИЯ: RUSSIAN  SNOWBOARD AWARDS </div>
							<p>Седьмая ежегодная премия индустрии российского сноубор-динга.</p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-7.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ФЕСТИВАЛЬ: X-MOTION-2013</div>
							<p>Уникальная возможность заняться степом и кросс-фитом под руководством лучших...  </p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-8.png" alt="" /></div>
						</a>
					</li>
					<li>
						<a href="#">
						<div class="text-b">
							<div class="subject-b">ПРЕМИЯ: RUSSIAN  SNOWBOARD AWARDS </div>
							<p>Седьмая ежегодная премия индустрии российского сноубор-динга.</p>
						</div>
						<div class="img-b"> <img src="img/img-item-circle-7.png" alt="" /></div>
						</a>
					</li>
				</ul>
				<div class="live-slider-btn-navig clearfix">
					<div id="slide-live-list-prev"></div>
					<div id="slide-live-list-next"></div>
				</div>
			</div>
		</div>
	</div><!-- content -->
</div><!-- wrapper-b -->
</body>
</html>
