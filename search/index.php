<?php

require_once('../com/db.php');
require_once('../com/config.php');

$html_root=$config['html_root'];

$db=new database();
$db->connect();

// if (isset($_GET['offset']))
// {
// 	$offset=intval($_GET['offset']);
// 	echo $offset;
// 	die();
// }

$search_query=$_GET['q'];

$categories=array();
$qcat=$db->query('SELECT * FROM ecrm_category WHERE category_parent=0 and category_id>1');
while ($cat=$db->fetch($qcat)) {
  //$prefix='';
  //if (intval($cat['category_type'])==1) $prefix='ИГРА --> ';
  $categories[]=array('id'=>$cat['category_id'],'name'=>$cat['category_name']);
  $qcat2=$db->query('SELECT * FROM ecrm_category WHERE category_parent='.intval($cat['category_id']));
  while ($cat2=$db->fetch($qcat2)) {
    $categories[]=array('id'=>$cat2['category_id'],'name'=>$cat2['category_name']);
  }
}

$qcat1=$db->query('SELECT * FROM ecrm_category WHERE category_parent=0 ORDER BY category_position');
while ($cat1=$db->fetch($qcat1)) {
  $categories_menu[intval($cat1['category_type'])][]=$cat1;
  $cat_names[$cat1['category_id']]=$cat1['category_name'];
}

$qcur=$db->query('SELECT * FROM ecrm_category WHERE category_id='.intval($_GET['s']));
$current=$db->fetch($qcur);
$cfg=json_decode($current['category_config'],true);

function autoparagraph($par,$num)
{
	if (mb_strlen($par,'UTF-8')>$num)
	{
		$space_place=0;
		for ($i=$num; $i >0 ; $i--) { 
			if (mb_substr($par, $i, 1,'UTF-8')==' ')
			{
				$space_place=$i;
				break;
			}
		}
		if ($space_place>0) $par=mb_substr($par, 0, $space_place,'UTF-8').'</span></li><li><span>'.mb_substr($par, $space_place+1,mb_strlen($par,'UTF-8'));
	}
	return $par;
}
/*
{
  "slider1_title": "NFL:",
  "slider1_text": "БРУКЛИН НЕ ОСТАВИЛ ЯНКИС\r\nНИ ОДНОГО ШАНСА НА ВЫХОД В ПЛЕЙ-ОФФ",
  "slider1_link": "test",
  "slider1": "temp/croppedImg_741743590.jpeg",
  "slider2_title": "NFL:",
  "slider2_text": "БРУКЛИН НЕ ОСТАВИЛ ЯНКИС",
  "slider2_link": "test2",
  "slider2": "temp/croppedImg_2143759933.jpeg",
  "slider3_title": "",
  "slider3_text": "",
  "slider3_link": "",
  "slider3": "",
  "slider4_title": "",
  "slider4_text": "",
  "slider4_link": "",
  "slider4": "",
  "slider5_title": "",
  "slider5_text": "",
  "slider5_link": "",
  "slider5": "",
  "number_title": "147 000",
  "number_text": "ЧЕЛОВЕК ПОСЕТИЛИ МАТЧ \r\nПО АМЕРИКАНСКОМУ ФУТБОЛУ \r\nДАЛЛАС - КОЛОРАДО \r\n7 МАРТА 2007 ГОДА",
  "number_link": "test_number",
  "date_title": "20 ФЕВРАЛЯ",
  "date_text": "",
  "date_link": "test_date"
}
*/

$_GET['q']=mb_strtolower($_GET['q'],'UTF-8');

/*$qnews=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=13 and (LOWER(stuff_tags) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(widget_desc) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_head_title) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_geo) LIKE "%'.addslashes($_GET['q']).'%") ORDER BY stuff_id DESC LIMIT 5');
$news=array();
while ($nnews=$db->fetch($qnews))
{
	$news[]=$nnews;
}
*/
$total_count=0;
//echo '<!--SELECT * FROM ecrm_stuff WHERE widget_type=1 and (LOWER(stuff_tags) LIKE "'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['q']).'" OR LOWER(widget_desc) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_head_title) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_geo) LIKE "%'.addslashes($_GET['q']).'%") ORDER BY stuff_id DESC LIMIT 7-->';

$offset=intval($_GET['offset'])*6;
if (mb_strlen($_GET['q'],'UTF-8')>2)
{
	$sparam='q='.urlencode($_GET['q']);

	$qitemres=$db->query('SELECT * FROM ecrm_stuff WHERE (LOWER(stuff_tags) LIKE "'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['q']).'" OR LOWER(widget_desc) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_head_title) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_geo) LIKE "%'.addslashes($_GET['q']).'%") ORDER BY stuff_id DESC LIMIT '.$offset.',6');
	$ressults=array();
	while ($nres=$db->fetch($qitemres)) 
	{ 
		$ressults[]=$nres; 
		$total_count++; 
		//echo '<!--'.$total_count.'-->';
	} 

	$qitem1=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=1 and (LOWER(stuff_tags) LIKE "'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['q']).'" OR LOWER(widget_desc) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_head_title) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_geo) LIKE "%'.addslashes($_GET['q']).'%") ORDER BY stuff_id DESC LIMIT 7');
	$item1=array();
	while ($nitem1=$db->fetch($qitem1)) 
	{ 
		$item1[]=$nitem1; 
		$total_count++; 
		//echo '<!--'.$total_count.'-->';
	} 
 //echo '<!--count:'.$db->count().'-->';
	$qitem5=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=5 and (LOWER(stuff_tags) LIKE "'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['q']).'" OR LOWER(widget_desc) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_head_title) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_geo) LIKE "%'.addslashes($_GET['q']).'%") ORDER BY stuff_id DESC LIMIT 2');
	$item5=array(); 
	while ($nitem5=$db->fetch($qitem5)) 
	{ 
		$item5[]=$nitem5; 
		$total_count++; 
	} 
 
	$qitem6=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=6 and (LOWER(stuff_tags) LIKE "'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['q']).'" OR LOWER(widget_desc) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_head_title) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_geo) LIKE "%'.addslashes($_GET['q']).'%") ORDER BY stuff_id DESC LIMIT 2');
	$item6=array(); 
	while ($nitem6=$db->fetch($qitem6)) 
	{ 
		$item6[]=$nitem6; 
		$total_count++; 
	} 
 
	$qitem4=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=4 and (LOWER(stuff_tags) LIKE "'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['q']).'" OR LOWER(widget_desc) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_head_title) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_geo) LIKE "%'.addslashes($_GET['q']).'%") ORDER BY stuff_id DESC LIMIT 6');
	$item4=array();
	while ($nitem4=$db->fetch($qitem4))
	{
		$item4[]=$nitem4;
		$total_count++;
	}

	$qitem11=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=11 and (LOWER(stuff_tags) LIKE "'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['q']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['q']).'" OR LOWER(widget_desc) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_head_title) LIKE "%'.addslashes($_GET['q']).'%" OR LOWER(stuff_geo) LIKE "%'.addslashes($_GET['q']).'%") ORDER BY stuff_id DESC LIMIT 3');
	$item11=array();
	while ($nitem11=$db->fetch($qitem11))
	{
		$item11[]=$nitem11;
		$total_count++;
	}
}
elseif (mb_strlen($_GET['tag'])>2)
{
	$sparam='tag='.urlencode($_GET['tag']);
	$_GET['tag']=urldecode($_GET['tag']);
	$search_query=$_GET['tag'];

	$qitemres=$db->query('SELECT * FROM ecrm_stuff WHERE (LOWER(stuff_tags) LIKE "'.addslashes($_GET['tag']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['tag']).'") ORDER BY stuff_id DESC LIMIT '.$offset.',6');
	$ressults=array();
	while ($nres=$db->fetch($qitemres)) 
	{ 
		$ressults[]=$nres; 
		$total_count++; 
		//echo '<!--'.$total_count.'-->';
	} 


	$qitem1=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=1 and (LOWER(stuff_tags) LIKE "'.addslashes($_GET['tag']).',%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['tag']).'") ORDER BY stuff_id DESC LIMIT 7');
	$item1=array();
	while ($nitem1=$db->fetch($qitem1))
	{
		$item1[]=$nitem1;
		$total_count++;
	}

	$qitem5=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=5 and (LOWER(stuff_tags) LIKE "%'.addslashes($_GET['tag']).'%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['tag']).'") ORDER BY stuff_id DESC LIMIT 2');
	$item5=array();
	while ($nitem5=$db->fetch($qitem5))
	{
		$item5[]=$nitem5;
		$total_count++;
	}

	$qitem6=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=6 and (LOWER(stuff_tags) LIKE "%'.addslashes($_GET['tag']).'%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['tag']).'") ORDER BY stuff_id DESC LIMIT 2');
	$item6=array();
	while ($nitem6=$db->fetch($qitem6))
	{
		$item6[]=$nitem6;
		$total_count++;
	}

	$qitem4=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=4 and (LOWER(stuff_tags) LIKE "%'.addslashes($_GET['tag']).'%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['tag']).'") ORDER BY stuff_id DESC LIMIT 6');
	$item4=array();
	while ($nitem4=$db->fetch($qitem4))
	{
		$item4[]=$nitem4;
		$total_count++;
	}

	$qitem11=$db->query('SELECT * FROM ecrm_stuff WHERE widget_type=11 and(LOWER(stuff_tags) LIKE "%'.addslashes($_GET['tag']).'%" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).'" OR LOWER(stuff_tags) LIKE "%,'.addslashes($_GET['tag']).',%" OR LOWER(stuff_tags) = "'.addslashes($_GET['tag']).'") ORDER BY stuff_id DESC LIMIT 3');
	$item11=array();
	while ($nitem11=$db->fetch($qitem11))
	{
		$item11[]=$nitem11;
		$total_count++;
	}
}

if (isset($_GET['offset']))
{
	$offset=intval($_GET['offset']);
	echo '						<div class="tabs-content-b">
							<ul id="tabNews-1" class="clearfix" style="display: block">';
	foreach ($ressults as $key => $value) {
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type4.png';
			else $value['widget_img']=$value['stuff_img'];
		}
		else{
			$value['widget_img']='/admin/stuff/'.$value['widget_img'];
		}
		echo '							<li>
									<div class="img-b"> <img src="'.$value['widget_img'].'" alt="'.$value['widget_desc'].'"></div>
									<div class="text-b" style="width: 540px;">
										<div class="subject-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_desc'].'</a> </div>
										<div class="params-b clearfix">
											<div class="date-b">'.date('d.m',$value['stuff_date']).'</div>
											<div class="location-b"> <i></i> <span>'.$value['stuff_geo'].'</span></div>
										</div>
									</div>
								</li>';
	}
	echo '</ul></div>';
	//echo $offset;
	die();
}

//echo '<!--total:'.$total_count.'-->';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="shortcut icon" href="/favicon.png">
	<link rel="stylesheet" type="text/css" href="../css/style.css" />
	<script src="../js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="../js/jquery.bxslider.js" type="text/javascript"></script>
	<script src="../js/jquery.interactive_bg.js" type="text/javascript"></script>
	<script src="../js/myScript.js" type="text/javascript"></script>
<title>А ЛИГА &copy; <?=((isset($titles[$current['category_id']]))?$titles[$current['category_id']]:$current['category_name']);?></title>
</head>

<body>
<div class="wrapper-b">
	<div class="header-b relative-b clearfix">
		<a class="logo-b" href="<?=$html_root;?>">a-liga</a>
		<div class="row">
			<div class="col-8">
				<div class="ml-95">
					<div class="quote-b">            <?
              foreach ($categories_menu[0] as $key => $cat) {
                echo '<span'.(($key>0)?' style="opacity: 0.2;"':'').'>'.$cat['category_name'].'</span> ';
              }
            ?></div>
          <ul class="header-nav-b clearfix">
            <?
              foreach ($categories_menu[1] as $key => $cat) {
              	if ($cat['category_id']!=intval($_GET['s']))
              	{
              		echo '<li><a href="'.$html_root.'sport/'.intval($cat['category_id']).'">'.$cat['category_name'].'</a></li> ';
              	}
                else
                {
                	echo '<li class="active" style="font-size: 12.4px;">'.$cat['category_name'].'</li> ';
                }
              }
              ?>
					</ul>
				</div>
			</div>
			<div class="col-4">
				<form class="search-header-b bd-7-yellow pull-right" action="<?=$html_root.'search/';?>" method="get">
					<input class="pull-right" type="text" name="q" id="" placeholder="" value="<?=htmlspecialchars($search_query)?>"/>
					<input type="submit" value="send" />
				</form>
			</div>
		</div>
		<div class="line-header-gray pull-right"></div>
	</div><!-- header-b -->
	
	<div class="content-b">
		
		<div class="container info-block-type2">
			<div class="text-b">
				<div class="subject-b clearfix">
					<span>ПОИСК: <?=htmlspecialchars(mb_substr(mb_strtoupper($search_query,'UTF-8'), 0,30)).((mb_strlen($search_query,'UTF-8')>30)?'...':'');?></span>
				</div>
				<?=($total_count==0?'<div class="subject-b clearfix"><span>НИЧЕГО НЕ НАЙДЕНО</span></div>':'')?>
				<ul class="params-b clearfix">
<?
	echo $counts[$current['category_id']];
	/*
					<li>
						<div class="nr-b">3567</div>
						<p>ИГРОКОВ</p>
					</li>
					<li>
						<div class="nr-b">412</div>
						<p>КОММАНД</p>
					</li>
					<li>
						<div class="nr-b">345</div>
						<p>ФАН КЛУБОВ</p>
					</li>
					<li>
						<div class="nr-b">1</div>
						<p>ПРАВИЛА</p>
					</li>
	*/
?>

					
				</ul>
				
				<div class="breadcrumbs-b" style="visibility: hidden; height: 0px;">
					<a href="#">&nbsp; </a>
					<a href="#">&nbsp;</a>
				</div>
			</div>
			<img src="<?=$html_root.'img/search.jpg';?>" alt="img" />
		</div><!-- info-block-type1 -->
<?
/*
		<div class="container">
			<div class="wr-statistic-slider-b">
				<ul class="statistic-slider-b">
					<li>
						<div class="img-left">
							<img src="../img/ico-team-1.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">100</div>
							<div class="nr-right">99</div>
						</div>
						<div class="img-right">
							<img src="../img/ico-team-2.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="../img/ico-team-3.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">55</div>
							<div class="nr-right">45</div>
						</div>
						<div class="img-right">
							<img src="../img/ico-team-4.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="../img/ico-team-5.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">35</div>
							<div class="nr-right">99</div>
						</div>
						<div class="img-right">
							<img src="../img/ico-team-6.png" alt="img" />
						</div>
					</li>
					<li>
						<div class="img-left">
							<img src="../img/ico-team-3.png" alt="img" />
						</div>
						<div class="stat-b">
							<div class="nr-left">112</div>
							<div class="nr-right">87</div>
						</div>
						<div class="img-right">
							<img src="../img/ico-team-1.png" alt="img" />
						</div>
					</li>
				</ul>
				<div class="statistic-slider-btn-navig clearfix">
					<div id="statistic-slider-prev"></div>
					<div id="statistic-slider-next"></div>
				</div>
			</div><!-- wr-statistic-slider-b -->
		</div><!-- container -->
*/
		?>		
<?
/*

		
		<div class="row container" <?=($total_count==0?' style="visibility: hidden; height: 0px;"':'');?>>
			<div class="col-8">
				<div class="row">
<?
foreach ($item1 as $key => $value) {
	if ($key<2)
	{
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
		echo'					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bg-blue text-top">
									<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>
								<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>
							</div>
						</div>
					</div>';
	}
}
?>
				</div>
				<div class="row mt-25" <?=($total_count==0?' style="visibility: hidden; height: 0px;"':'');?>>

<?
$value=$item5[0];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']='/'.$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>
					</div>
				</div>
			</div>
';

?>

<?
$value=$item6[0];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
		else $value['widget_img']='/'.$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'"><img src="'.$value['widget_img'].'" alt="img" width="194" height="194" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_title'].'</a> </div>
						<p>'.$value['widget_desc'].'</p>
					</div>
				</div>
';
?>

					</div>
				</div>
			</div>
			<div class="col-4">
				<div class="bd-4-gray_wo_border">
					<a href="<?=$cfg['banner_link'];?>"><img src="/admin/category/<?=$cfg['banner'];?>" width="308" height="508" alt="img" /></a>
				</div>
			</div>
		</div><!-- container -->
		<div class="bd-12-yellow mt-25" <?=($total_count==0?' style="visibility: hidden; height: 0px;"':'');?>>
			<div class="wr-slide-live-list-b">
				<ul class="slide-live-list-b live-list-b clearfix">
<?
for ($i=0;$i<3;$i++)
{
$value=$item11[$i];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
			else $value['widget_img']=$value['stuff_img'];
		}	
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
	echo '				<li>
					<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
					<div class="text-b">
						<div class="subject-b">'.$value['widget_title'].'</div>
						<p>'.$value['widget_desc'].'</p>
					</div>
					<div class="img-b"> <img src="'.$value['widget_img'].'" alt="" /></div>
					</a>
				</li>
';
}

?>

				</ul>
				<!--<div class="live-slider-btn-navig clearfix">
					<div id="slide-live-list-prev"></div>
					<div id="slide-live-list-next"></div>
				</div>-->
			</div>
		</div>		
*/
?>
		

		
		<div class="container" <?=(($total_count==0)|(count($item1)==0&&count($item4)==0)?'style=" visibility: hidden; height: 0px;"':'');?>>


				<div class="col-8" style="padding-left: 110px;">
						<div class="tabs-content-b">
							<ul id="tabNews-1" class="clearfix" style="display: block">
<?
foreach ($ressults as $key => $value) {
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type4.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
	echo '							<li>
								<div class="img-b"> <img src="'.$value['widget_img'].'" alt="'.$value['widget_desc'].'"></div>
								<div class="text-b" style="width: 540px;">
									<div class="subject-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_desc'].'</a> </div>
									<div class="params-b clearfix">
										<div class="date-b">'.date('d.m',$value['stuff_date']).'</div>
										<div class="location-b"> <i></i> <span>'.$value['stuff_geo'].'</span></div>
									</div>
								</div>
							</li>';
}
?>
							
							</ul>
						</div>

		<a href="#" class="show-more">
		<div class="bd-4-gray mt-25 showmore">
			<img src="/img/more.png" alt="Показать еще" title="Показать еще"/>
		</div>
		</a>

				</div>



			<div class="wr-video-slider-b mt-25">
				<ul class="video-slider-b">
<?

foreach ($item1 as $key => $value) {
	if ($key<6)
	{
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
		echo'					<div class="col-4">
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bg-blue text-top">
									<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="bg-paralax h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
								<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.intval($value['stuff_views']).'</span></a></div>
							</div>
						</div>
					</div>';
	}
}

foreach ($item4 as $key => $value) {
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type4.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}

echo '					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="'.$html_root.'video/'.$value['stuff_id'].'">
										<ul>
											<li><span class="text-underline">'.$value['widget_title'].'</span></li>
											<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'video/'.$value['stuff_id'].'"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
								<div class="date-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
								<!--<div class="like-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>0</a></span></div>-->
								<div class="view-b"><a href="'.$html_root.'video/'.$value['stuff_id'].'"><i></i> <span>'.intval($value['stuff_views']).'</a></span></div>
							</div>
						</div>
					</li>
					';
}
?>
<?/*
					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="#">
										<ul>
											<li><span class="text-underline">СУББОТА:</span></li>
											<li><span>НЕОЖИДАННАЯ КОНЦОВКА МАТЧА</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="../img/img-item-11.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ВИДЕО</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</a></span></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</a></span></div>
							</div>
						</div>
					</li>
					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="#">
										<ul>
											<li><span class="text-underline">FASHION:</span></li>
											<li><span>HOODY SHOW BY CHICAGO BULLS</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="../img/img-item-21.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ВИДЕО</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</li>
					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline">РЕКЛАМА:</span></li>
											<li><span>ASKET JINGLE ALL THE WAY !</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="../img/img-item-13.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ВИДЕО</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</li>
					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom text-bg-green">
									<a href="#">
										<ul>
											<li><span class="text-underline">FASHION:</span></li>
											<li><span>HOODY SHOW BY CHICAGO BULLS</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="../img/img-item-21.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ВИДЕО</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</li>
					<li>
						<div class="main-item-view">
							<div class="img-b">
								<div class="text-b text-bottom">
									<a href="#">
										<ul>
											<li><span class="text-underline">РЕКЛАМА:</span></li>
											<li><span>ASKET JINGLE ALL THE WAY !</span></li>
										</ul>
									</a>
								</div>
								<a class="video-b bg-paralax w-307 h-200" data-ibg-bg="../img/img-item-13.jpg" href="#"></a>
							</div>
							<div class="params-b">
								<div class="name-b"><a href="#">ВИДЕО</a></div>
								<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
								<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
								<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
							</div>
						</div>
					</li>*/
?>
				</ul>
				<div class="video-slider-btn-navig clearfix">
					<div id="video-slider-prev"></div>
					<div id="video-slider-next"></div>
				</div>
			</div><!-- wr-statistic-slider-b -->
		</div><!-- container -->
		
		<div class="row container" <?=($total_count==0|(count($item6)==0&&count($item1)<7&&count($item5)==0)?' style="visibility: hidden; height: 0px;"':'');?>>
<?

$value=$item6[0];
//print_r($value);
//die();
error_reporting(LC_ALL);
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type6.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="'.$html_root.'inside/'.$value['stuff_id'].'"><img src="'.$value['widget_img'].'" alt="img" width="194" height="194" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="'.$html_root.'inside/'.$value['stuff_id'].'">'.$value['widget_title'].'</a> </div>
						<p>'.$value['widget_desc'].'</p>
					</div>
				</div>

';
/*
			<div class="w-200 ml-10 mr-10 pull-left">
				<div class="main-item-view2">
					<div class="img-b"> <a href="#"><img src="../img/img-item-circle-1.png" alt="img" /></a></div>
					<div class="text-b">
						<div class="subject-b"> <a class="text-underline" href="#">НАСТРОЙ</a> </div>
						<p>Первородная мотивация от секс символов современности</p>
					</div>
				</div>
			</div>
*/
?>
</div>
			
<?
$value=$item5[1];
	if (strlen($value['widget_img'])==0)
	{
		if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type3.png';
		else $value['widget_img']=$value['stuff_img'];
	}
	else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo'
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],45).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"></a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.intval($value['stuff_views']).'</span></a></div>
					</div>
				</div>
			</div>
';
/*
			<div class="w-416 ml-10 mr-10 pull-left">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom text-bg-blue">
							<a href="#">
								<ul>
									<li><span class="text-underline text-color-yellow">КИНО:</span></li>
									<li><span>БЕЛЫЕ НЕ УМЕЮТ ПРЫГАТЬ</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="../img/img-item-22.jpg" href="#"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="#">ТАЙМ - АУТ</a></div>
						<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
						<div class="date-b"><a href="#">KINOPOISK 7,8</a></div>
						<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
						<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
					</div>
				</div>
			</div>
*/
?>

			
<?

$value=$item1[6];
		if (strlen($value['widget_img'])==0)
		{
			if (strlen($value['stuff_img'])==0) $value['widget_img']='/admin/img_uploads/type1.png';
			else $value['widget_img']=$value['stuff_img'];
		}
			else{
		$value['widget_img']='/admin/stuff/'.$value['widget_img'];
	}
echo			
'			<div class="col-4">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="'.$html_root.'inside/'.$value['stuff_id'].'">
								<ul>
									<li><span class="text-underline text-color-yellow">'.$value['widget_title'].'</span></li>
									<li><span>'.autoparagraph($value['widget_desc'],28).'</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="'.$value['widget_img'].'" href="'.$html_root.'inside/'.$value['stuff_id'].'"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="'.$html_root.'sport/'.$value['category_id'].'">'.$cat_names[$value['category_id']].'</a></div>
						<div class="date-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'">'.date('d.m',$value['stuff_date']).'</a></div>
						<!--<div class="like-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>0</span></a></div>-->
						<div class="view-b"><a href="'.$html_root.'inside/'.$value['stuff_id'].'"><i></i> <span>'.intval($value['stuff_views']).'</span></a></div>
					</div>
				</div>
			</div>';
/*
			<div class="col-4">
				<div class="main-item-view">
					<div class="img-b">
						<div class="text-b text-bottom">
							<a href="#">
								<ul>
									<li><span class="text-underline text-color-yellow">ПО МЕСТАМ:</span></li>
									<li><span>ИСТОРИЯ ВЕЛИКОГО СТАДИОНА</span></li>
									<li><span>СЕНТ-ЛУИС</span></li>
								</ul>
							</a>
						</div>
						<a class="bg-paralax h-250" data-ibg-bg="../img/img-item-15.jpg" href="#"></a>
					</div>
					<div class="params-b">
						<div class="name-b"><a href="#">ТАЙМ - АУТ</a></div>
						<div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
						<div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
						<div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
					</div>
				</div>
			</div>
*/
?>

		</div><!-- row container -->


	</div><!-- content -->
	
	<div class="footer-b container clearfix">
        <a href="" class="logo-g"><img src="../img/logo-gray.png" alt="logo" /></a>
        <div class="copyright-b">
            <span>&copy; 2014. А ЛИГА.</span>
            <p>Интернет-издание об игровых <br />видах спорта, активном образе <br />жизни и не только.</p>
        </div>

        <div class="text-b">
        	<div class="footer-b__social pull-right">
        		<p class="pull-left ttl-b"> <span>FOLLOW US</span> <img src="../img/ico-a.png" alt="" /></p>
        		<ul class="pull-right">
        			<li><a class="ico-b i-tw" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-y" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-gp" href="#" style="visibility: hidden;"></a></li>
        			<li><a class="ico-b i-vk" href="http://vk.com/a.league" target="_blank"></a></li>
        			<li><a class="ico-b i-fb" href="http://facebook.com/aleagueru" target="_blank"></a></li>
        			<li><a class="ico-b i-pr" href="http://instagram.com/a.league" target="_blank"></a></li>
        		</ul>
        	</div>
            
            <ul class="footer-b__nav pull-right">
                <li><a href="/inside/530">ПОМОЩЬ</a></li>
                <li><a href="/inside/529">КОНТАКТЫ</a></li>
                <li><a href="/inside/532">ПРАВА</a></li>
                <li><a href="/inside/533">РЕКЛАМОДАТЕЛЯМ</a></li>
            </ul>
        </div>
	</div><!-- content -->
</div><!-- wrapper-b -->
<script type="text/javascript">
var show_offset=0;
$('.show-more').click(function() {
	show_offset++;
	$.ajax({
  url: "?<?=$sparam;?>&offset="+show_offset
})
  .done(function( data ) {
  	$('.show-more').before(''+data+'');
  });
  return false;
});
</script>
</body>
</html>