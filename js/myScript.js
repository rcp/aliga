$(document).ready(function() {
	
	//paralax
	if ($(".bg-paralax").interactive_bg!=undefined)
	{
		$(".bg-paralax").interactive_bg({
		   strength: 15,
		   scale: 1.05,
		   animationSpeed: "100ms",
		   contain: true,
		   wrapContent: true
		 });
	}

	
	//change tabs news
	$('.main-news-b .tabs-b').on('click', 'a', function(e){
		$thisA = $(this);
		arrTab = $thisA.attr('href').split('-');
		$thisA.addClass('active').siblings('a').removeClass('active');
		$('.tabs-content-b #tabNews-'+arrTab[1]).show().siblings('ul').hide();
		e.preventDefault();
	});
	
	//sliders
	$('.main-slider-b').bxSlider({
		slideWidth: 636,
		pager: true,
		controls: false,
		pagerSelector: '#main-slider-pager-b',
		//nextSelector: '#main-slider-next',
		//prevSelector: '#main-slider-prev',
		infiniteLoop: false,
		minSlides: 1,
		maxSlides: 1,
		auto: true,
		pause: 3000,
		autoHover: true,
		autoDelay: 300
	});
	
	$('.statistic-slider-b').bxSlider({
		slideWidth: 295,
		pager: false,
		nextSelector: '#statistic-slider-next',
		prevSelector: '#statistic-slider-prev',
		infiniteLoop: false,
		minSlides: 3,
		maxSlides: 3,
		slideMargin: 20,
		auto: true,
		pause: 5000,
		autoHover: true,
		autoDelay: 5000,
		moveSlides: 3
	});
	
	$('.video-slider-b').bxSlider({
		slideWidth: 307,
		pager: false,
		nextSelector: '#video-slider-next',
		prevSelector: '#video-slider-prev',
		infiniteLoop: false,
		minSlides: 3,
		maxSlides: 3,
		slideMargin: 20,
		auto: true,
		pause: 5000,
		autoHover: true,
		autoDelay: 5000,
		moveSlides: 3
	});
	
	$('.slide-live-list-b').bxSlider({
		slideWidth: 285,
		pager: false,
		nextSelector: '#slide-live-list-next',
		prevSelector: '#slide-live-list-prev',
		infiniteLoop: false,
		minSlides: 3,
		maxSlides: 3,
		slideMargin: 20,
		auto: true,
		pause: 5000,
		autoHover: true,
		autoDelay: 5000,
		moveSlides: 3
	});
	
	$('.gallery-slider-b').bxSlider({
		slideWidth: 636,
		mode: 'fade',
		pager: true,
		pagerType: 'short',
		pagerSelector: '#gallery-slider-pager-b',
		nextSelector: '#gallery-slider-next',
		prevSelector: '#gallery-slider-prev',
		infiniteLoop: false,
		minSlides: 1,
		maxSlides: 1,
		auto: true,
		pause: 5000,
		autoHover: true,
		autoDelay: 5000,
		captions: true,
	});
});