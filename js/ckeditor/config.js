/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.font_names =
	'"roboto-light",Arial,sans-serif;' +
	'"roboto-black",Arial,sans-serif' +
    '"roboto-condensed", Arial,sans-serif;' +
    '"roboto-medium",Arial,sans-serif;' +
    '"roboto-bold", Arial,sans-serif;' +
    'Arial/Arial, Helvetica, sans-serif;' +
    'Times New Roman/Times New Roman, Times, serif;' +
    'Verdana';
    config.line_height="1em;1.1em;1.2em;1.3em;1.4em;1.5em" ;

config.oembed_maxWidth = '632';
config.oembed_maxHeight = '356';
config.doksoft_gallery_thumb_resize_show = false;
config.doksoft_gallery_default_thumb_enlarge = false;
config.doksoft_gallery_default_thumb_width = 634;
config.doksoft_gallery_default_thumb_height = 413;
config.doksoft_gallery_default_thumb_resize_width = 634;
config.doksoft_gallery_default_thumb_resize_height = 413;
config.doksoft_gallery_template =   
        "	<li>"+
							"<img src=\"{PREVIEW}\" title=\"&nbsp;\" />"+
						"</li>";
// config.doksoft_gallery_template_wrap = 
//         "	<div class='doksoft_gallery' style='display: table;line-height:0'>{ITEMS}</div>";
     //    					"<div class=\"gallery-slider-btn-navig clearfix\">"+
					// 	"<div id=\"gallery-slider-prev\"></div>"+
					// 	"<div id=\"gallery-slider-pager-b\"></div>"+
					// 	"<div id=\"gallery-slider-next\"></div>"+
					// "</div>"+
// config.doksoft_gallery_template =   
//         "<div class='doksoft_gallery_item' style='white-space: nowrap; display: inline-block;'>" +
//              "<div style='width:{PREVIEW_HEIGHT}px;height:{PREVIEW_HEIGHT}px;max-width:{PREVIEW_HEIGHT}px;overflow:hidden'>" +
//                   "<a rel='lightbox' href='{IMAGE}'>" +
//                         "<span style='display: inline-block; height: 100%; vertical-align: middle;'></span>" +
//                         "<img src='{PREVIEW}' style='vertical-align: middle'/>" +
//                    "</a>" +
//               "</div>" +
//         "</div>";]
 config.doksoft_gallery_template_wrap = 
        "<div class=\"wr-gallery-slider-b mb-10\"><ul class=\"gallery-slider-b\">{ITEMS}</ul><div class=\"gallery-slider-btn-navig clearfix\"><div id=\"gallery-slider-prev\"></div><div id=\"gallery-slider-pager-b\"></div><div id=\"gallery-slider-next\"></div></div></div>";

	config.extraPlugins = 'doksoft_gallery,tableresize,tabletools,blockquote,oembed,richcombo,lineheight';
	config.toolbar_name = [ [ 'doksoft_gallery' ] ];



					
};
