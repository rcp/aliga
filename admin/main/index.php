<?

require_once('../com/db.php');
require_once('../com/config.php');
require_once('../login.php');
require_once('../tpl/template.php');

$db=new database();
$db->connect();
// print_r($_POST);
// die();
login();

echo get_header('../');

echo '
    <div class="container">
    <ul class="breadcrumb">
      <li class="active">Главная</li>
    </ul>
    <legend>Статистика сайта</legend>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Параметр</th>
          <th>Неделя</th>
          <th>Месяц</th>
        </tr>
      </thead>
      <tbody>
      <tr>
        <td>
         Показы
        </td>
        <td>
         28374
        </td>
        <td>
         182740
        </td>
      </tr>
      <tr>
        <td>
         Посетители
        </td>
        <td>
         1278
        </td>
        <td>
         38971
        </td>
      </tr>
      <tr>
        <td>
         Новостей
        </td>
        <td>
         762
        </td>
        <td>
         2837
        </td>
      </tr>
      <tr>
        <td>
         Статей
        </td>
        <td>
         76
        </td>
        <td>
         283
        </td>
      </tr>
      </tbody>
    </div>';
/*
if ($_POST['act']=='add_company')
{
    if ((strtotime($_POST['company_start'])<strtotime($_POST['company_end']))&&(trim($_POST['company_name'])!=''))
    {
    	$qadd_company=$db->query('INSERT INTO ecrm_companies (company_name,company_start,company_end,company_host,company_vk_id) VALUES (\''.addslashes($_POST['company_name']).'\','.strtotime($_POST['company_start']).','.strtotime($_POST['company_end']).',\''.addslashes($_POST['company_host']).'\',\''.addslashes($_POST['company_vk_id']).'\')');
    	$id_add_company=$db->insert_id($qadd_company);
    	$qadd_uc=$db->query('INSERT INTO ecrm_uc (user_id,company_id) VALUES ('.$user['user_id'].','.$id_add_company.')');
        $qreverse[]='DELETE FROM ecrm_companies WHERE company_id='.$id_add_company;
        $qreverse[]='DELETE FROM ecrm_uc WHERE uc_id='.$db->insert_id($qadd_uc);
        $db->query('INSERT INTO `ecrm.ecrm_user_logs` (log_time,log_reverse_sql,user_id) VALUES ('.time().',\''.addslashes(json_encode($qreverse)).'\','.$user['user_id'].')');
    }
    else
    {
        echo '<script>alert(\'Дата начала больше даты конца! / Название комании пустое!\');</script>';
    }
}
elseif ($_POST['act']=='edit_company')
{
    if ((strtotime($_POST['company_start'])<strtotime($_POST['company_end']))&&(trim($_POST['company_name'])!=''))
    {
    	$qedit_company=$db->query('SELECT * FROM ecrm_companies WHERE company_id='.$_GET['company_id']);
    	$edit_company=$db->fetch($qedit_company);
    	foreach ($edit_company as $key => $value)
    	{
    	    $params.=$zap.$key.'=\''.addslashes($value).'\'';
            $zap=',';
    	}
    	// print_r($_POST);
    	$q=$db->query('UPDATE ecrm_companies SET company_name=\''.addslashes($_POST['company_name']).'\', company_start='.strtotime($_POST['company_start']).' ,company_end='.strtotime($_POST['company_end']).' ,company_host=\''.addslashes($_POST['company_host']).'\' ,company_vk_id=\''.addslashes($_POST['company_vk_id']).'\' WHERE company_id='.$_GET['company_id']);
    	$qreverse[]='UPDATE ecrm_companies SET '.$params.'WHERE company_id='.$_GET['company_id'];
        $db->query('INSERT INTO `ecrm.ecrm_user_logs` (log_time,log_reverse_sql,user_id) VALUES ('.time().',\''.addslashes(json_encode($qreverse)).'\','.$user['user_id'].')');
    }
    else
    {
        echo '<script>alert(\'Дата начала больше даты конца! / Название комании пустое!\');</script>';
    }
}
elseif ($_GET['del_company_id']!='')
{
	$qdel_company=$db->query('SELECT * FROM ecrm_companies WHERE company_id='.$_GET['del_company_id']);
	$del_company=$db->fetch($qdel_company);
	// print_r($del_company);
	foreach ($del_company as $key => $item)
	{
        $params.=$paramzap.$key;
        $paramzap=',';
        $values.=$valuezap.'\''.addslashes($item).'\'';
        $valuezap=',';
	}
	$qreverse[]='INSERT INTO ecrm_companies ('.$params.') VALUES ('.$values.')';
	// echo 'SELECT * FROM ecrm_uc WHERE company_id='.$_GET['del_company_id'].' AND user_id='.$user['user_id'];
	$qdel_uc=$db->query('SELECT * FROM ecrm_uc WHERE company_id='.$_GET['del_company_id'].' AND user_id='.$user['user_id']);
	$del_uc=$db->fetch($qdel_uc);
    $params='';
    $paramzap='';
    $values='';
    $valuezap='';
	foreach ($del_uc as $key => $item)
	{
        $params.=$paramzap.$key;
        $paramzap=',';
        $values.=$valuezap.'\''.addslashes($item).'\'';
        $valuezap=',';
	}
	$qreverse[]='INSERT INTO ecrm_uc ('.$params.') VALUES ('.$values.')';

	$qdel=$db->query('DELETE FROM ecrm_companies WHERE company_id='.$_GET['del_company_id']);
	$qdel=$db->query('DELETE FROM ecrm_uc WHERE company_id='.$_GET['del_company_id'].' AND user_id='.$user['user_id']);
	$db->query('INSERT INTO `ecrm.ecrm_user_logs` (log_time,log_reverse_sql,user_id) VALUES ('.time().',\''.addslashes(json_encode($qreverse)).'\','.$user['user_id'].')');
}
elseif ($_GET['start_company_id']!='')
{
    $qactive=$db->query('SELECT * FROM ecrm_companies WHERE company_id='.$_GET['start_company_id'].' LIMIT 1');
    $active_comp=$db->fetch($qactive);
    if ($active_comp['company_active']==0) $db->query('UPDATE ecrm_companies SET company_active=1 WHERE company_id='.$_GET['start_company_id']);
    else $db->query('UPDATE ecrm_companies SET company_active=0 WHERE company_id='.$_GET['start_company_id']);
}
elseif ($_GET['del_au_id']!='')
{
    $db->query('DELETE FROM ecrm_active_users WHERE company_id='.intval($_GET['del_au_id']));
}

$qcampanies=$db->query('SELECT * FROM ecrm_users as a LEFT JOIN ecrm_uc as b ON a.user_id=b.user_id LEFT JOIN ecrm_companies as c ON b.company_id=c.company_id WHERE a.user_id='.$user['user_id']);

echo '
    <div class="container">
    <ul class="breadcrumb">
	  <li class="active">Главная</li>
	</ul>
    <legend>Список кампаний</legend>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Название</th>
          <th>Начало кампании</th>
          <th>Конец кампании</th>
          <th>Действия</th>
        </tr>
      </thead>
      <tbody>
      ';
while ($campany=$db->fetch($qcampanies))
{
	if (($_GET['company_id']!='')&&($_GET['company_id']==$campany['company_id'])) $company_edit=$campany;
    if (intval($campany['company_id'])>0) 
        {
            echo '
    <tr>
        <td>'.$campany['company_id'].'</td>
        <td>'.$campany['company_name'].'</td>
        <td>'.date('d.m.Y',$campany['company_start']).'</td>
        <td>'.date('d.m.Y',$campany['company_end']).'</td>
        <td>
                <a href="?start_company_id='.$campany['company_id'].'"><button class="btn btn-small '.($campany['company_active']==1?'btn-success':'btn-danger').'"><i class="icon-off"></i></button></a>
                <div class="btn-group">
                  <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
                    Настройки
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="/admin/plans/?company_id='.$campany['company_id'].'">Расписание</a></li>
                    <li><a href="/admin/rules/?company_id='.$campany['company_id'].'">Стратегия</a></li>
                    <li><a href="/admin/target/?company_id='.$campany['company_id'].'">Целевые аудитории</a></li>
                    <li><a href="/admin/targetaction/?company_id='.$campany['company_id'].'">Целевые действия</a></li>
                    <li><a href="/admin/channels/?company_id='.$campany['company_id'].'">Каналы коммуникации</a></li>
                    <li><a href="/admin/users/?company_id='.$campany['company_id'].'">Активные участники</a></li>
                    <li><a href="/admin/lead/?company_id='.$campany['company_id'].'">Лиды</a></li>
                    <li><a href="/admin/stats/?company_id='.$campany['company_id'].'">Статистика по компании</a></li>
                    <li><a href="http://ecrm/import/upload.php?company_id='.$campany['company_id'].'">Импорт активных участников</a></li>
                  </ul>
                </div>
                <a href="/admin/company/export_audience.php?export_audience_company_id='.$campany['company_id'].'"><button class="btn btn-small btn-success"><i class="icon-arrow-down"></i></button></a>
                <a href="?del_au_id='.$campany['company_id'].'"><button class="btn btn-small btn-success">Очистить АУ</button></a>
                <a href="?company_id='.$campany['company_id'].'"><button class="btn btn-small btn-success"><i class="icon-pencil"></i></button></a>
                <a href="?del_company_id='.$campany['company_id'].'" onclick="return confirm(\'Удалить кампанию?\');"><button class="btn btn-small btn-danger" type="button"><i class="icon-remove"></i></button></a>
        </td>
    </tr>
    ';    
        }
}
echo '</tbody></table>';

// if ($_GET['company_id']!='')
{
	echo '<form method="POST" action="?company_id='.$_GET['company_id'].'">
	<input type="hidden" name="act" value="'.($_GET['company_id']!=''?'edit':'add').'_company">
	Название кампании: <input type="text" name="company_name" value="'.$company_edit['company_name'].'"><br>
	Начало периода: <input type="text" name="company_start" value="'.($company_edit['company_start']!=''?date('d-m-Y',$company_edit['company_start']):date('d').'-'.date('m').'-'.date('Y')).'" id="dp" data-date="'.($company_edit['company_start']!=''?date('d-m-Y',$company_edit['company_start']):date('d').'-'.date('m').'-'.date('Y')).'" data-date-format="dd-mm-yyyy"><br>
	Конец периода: <input type="text" name="company_end" value="'.($company_edit['company_end']!=''?date('d-m-Y',$company_edit['company_end']):date('d').'-'.date('m').'-'.date('Y')).'" id="dp1" data-date="'.($company_edit['company_end']!=''?date('d-m-Y',$company_edit['company_end']):date('d').'-'.date('m').'-'.date('Y')).'" data-date-format="dd-mm-yyyy"><br>
    Хост компании: <input type="text" name="company_host" value="'.$company_edit['company_host'].'"><br>
    ID группы вконтакте: <input type="text" name="company_vk_id" value="'.$company_edit['company_vk_id'].'"><br>
	<br><button type="submit" class="btn">'.($_GET['company_id']!=''?'Редактировать':'Добавить').'</button>
	</form>';
}

$script='$(\'#dp\').datepicker();$(\'#dp1\').datepicker();';
*/
echo get_footer('../',$script);

?>