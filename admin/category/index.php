<?

require_once('../com/db.php');
require_once('../com/config.php');
require_once('../login.php');
require_once('../tpl/template.php');

$db=new database();
$db->connect();
// print_r($_POST);
// die();
login();

if ($_POST['act']=='addstuff')
{
  //print_r($_POST);
  $c_config=array(
    'slider1_title'=>($_POST['slider1_title']),
    'slider1_text'=> ($_POST['slider1_text']),
    'slider1_link'=> ($_POST['slider1_link']),
    'slider1'=>      ($_POST['slider1']),
    'slider2_title'=>($_POST['slider2_title']),
    'slider2_text'=> ($_POST['slider2_text']),
    'slider2_link'=> ($_POST['slider2_link']),
    'slider2'=>      ($_POST['slider2']),
    'slider3_title'=>($_POST['slider3_title']),
    'slider3_text'=> ($_POST['slider3_text']),
    'slider3_link'=> ($_POST['slider3_link']),
    'slider3'=>      ($_POST['slider3']),
    'slider4_title'=>($_POST['slider4_title']),
    'slider4_text'=> ($_POST['slider4_text']),
    'slider4_link'=> ($_POST['slider4_link']),
    'slider4'=>      ($_POST['slider4']),
    'slider5_title'=>($_POST['slider5_title']),
    'slider5_text'=> ($_POST['slider5_text']),
    'slider5_link'=> ($_POST['slider5_link']),
    'slider5'=>      ($_POST['slider5']),
    'number_title'=>($_POST['number_title']),
    'number_text'=> ($_POST['number_text']),
    'number_link'=> ($_POST['number_link']),
    'date_title'=>($_POST['date_title']),
    'date_text'=> ($_POST['date_text']),
    'date_link'=> ($_POST['date_link']),
    'banner_link'=> ($_POST['banner_link']),
    'banner'=> ($_POST['banner'])
  );
  $c_config=json_encode($c_config);
  if (intval($_POST['category_id'])>0)
  {
    $db->query('UPDATE ecrm_category set category_name=\''.addslashes($_POST['category_name']).'\', category_config=\''.addslashes($c_config).'\', category_img=\''.addslashes($_POST['category_img']).'\', category_type=\''.intval($_POST['category_type']).'\' WHERE category_id='.intval($_POST['category_id']));
    //echo 'UPDATE ecrm_category set category_name=\''.addslashes($_POST['category_name']).'\', category_config=\''.addslashes($c_config).'\', category_img=\''.addslashes($_POST['category_img']).'\', category_type=\''.intval($_POST['category_type']).'\' WHERE category_id='.intval($_POST['category_id']);
  } else{
    $db->query('INSERT INTO ecrm_category (category_name,category_config,category_img,category_parent,category_type) VALUES (\''.addslashes($_POST['name']).'\',\''.$c_config.'\',\''.addslashes($_POST['category_img']).'\','.intval($_POST['category_parent']).','.intval($_POST['category_type']).')');
  }
}
elseif ($_GET['del_category_id']!='')
{
  $db->query('DELETE FROM ecrm_category WHERE category_id='.intval($_GET['del_category_id']));
}
elseif ($_GET['up_id']!='')
{
  $qnews=$db->query('SELECT * FROM ecrm_category WHERE category_id='.intval($_GET['up_id']).' LIMIT 1');
  $new=$db->fetch($qnews);
  $qnews2=$db->query('SELECT * FROM ecrm_category WHERE category_position<'.intval($new['category_position']).' ORDER BY category_position ASC LIMIT 1');
  $new2=$db->fetch($qnews2);
  $db->query('UPDATE ecrm_category SET category_position='.intval($new2['category_position']).' WHERE category_id='.intval($new['category_id']));
  //echo 'UPDATE ecrm_category SET category_position='.intval($new2['category_position']).' WHERE category_id='.intval($new['category_id']);
  $db->query('UPDATE ecrm_category SET category_position='.intval($new['category_position']).' WHERE category_id='.intval($new2['category_id']));
  //echo 'UPDATE ecrm_category SET category_position='.intval($new['category_position']).' WHERE category_id='.intval($new2['category_id']);
}
elseif ($_GET['down_id']!='')
{
  $qnews=$db->query('SELECT FROM ecrm_category WHERE category_id='.intval($_GET['down_id']).' LIMIT 1');
  echo 'SELECT * FROM ecrm_category WHERE category_id='.intval($_GET['down_id']).' LIMIT 1';
  $new=$db->fetch($qnews);
  //print_r($new);
  $qnews2=$db->query('SELECT FROM ecrm_category WHERE category_position>'.intval($new['category_position']).' ORDER BY category_position DESC LIMIT 1');
  echo 'SELECT * FROM ecrm_category WHERE category_position>'.intval($new['category_position']).' ORDER BY category_position ASC LIMIT 1';
  $new2=$db->fetch($qnews2);
  //print_r($new2);
  $db->query('UPDATE ecrm_category SET category_position='.intval($new2['category_position']).' WHERE category_id='.intval($new['category_id']));
  //echo 'UPDATE ecrm_category SET category_position='.intval($new2['category_position']).' WHERE category_id='.intval($new['category_id']);
  $db->query('UPDATE ecrm_category SET category_position='.intval($new['category_position']).' WHERE category_id='.intval($new2['category_id']));
  //echo 'UPDATE ecrm_category SET category_position='.intval($new['category_position']).' WHERE category_id='.intval($new2['category_id']);
}

echo get_header('../');

if (intval($_GET['edit_category'])>0)
{
  $qedit=$db->query('SELECT * FROM ecrm_category WHERE category_id='.intval($_GET['edit_category']).' LIMIT 1');
  $edit=$db->fetch($qedit);
  $edit['category_config']=json_decode($edit['category_config'],true);
  //print_r($edit['category_config']);
}
$catname=array(0=>'Верхнее',1=>'Нижнее');
echo '
    <div class="container">
    <ul class="breadcrumb">
      <li class="active">Главная</li>
    </ul>
    <legend>Разделы</legend>
    <form method="POST">
    <input type="hidden" name="act" value="addstuff">
    <input type="hidden" name="category_parent" value="'.intval($_GET['category_id']).'">
    <input type="hidden" name="category_id" value="'.intval($_GET['edit_category']).'">
    Название: <input type="text" name="category_name" value="'.$edit['category_name'].'"><br>
    Тип раздела: <select name="category_type">
    <option value="0" '.((intval($edit['category_type'])==0)?'selected':'').'>Верхнее меню</option>
    <option value="1" '.((intval($edit['category_type'])==1)?'selected':'').'>Нижнее меню</option>
    </select><br/>
    Обложка раздела:
    <div id="cropContainerEyecandy" style="width:961px; height:321px; position: relative; border:1px solid #ccc;'.(strlen($edit['category_img'])>0?' background: url(\''.$edit['category_img'].'\') no-repeat center;':'').'"></div><br/>
    <input type="hidden" id="myOutputId" name="category_img" value="'.$edit['category_img'].'"/>
    
    <h3>Главный слайдер:</h3>
    Заголовок: <input type="text" name="slider1_title" value="'.$edit['category_config']['slider1_title'].'"><br/>
    Текст: <textarea name="slider1_text" cols="80" rows="2">'.$edit['category_config']['slider1_text'].'</textarea><br/>
    Ссылка: <input type="text" name="slider1_link" value="'.$edit['category_config']['slider1_link'].'"><br/>
    <div id="cropContainerSlider1" style="width:636px; height:401px; position: relative; border:1px solid #ccc;'.(strlen($edit['category_config']['slider1'])>0?' background: url(\''.$edit['category_config']['slider1'].'\') no-repeat center;':'').'"></div><br/>
    <input type="hidden" id="SliderId1" name="slider1" value="'.$edit['category_config']['slider1'].'"/><br/></br>

    Заголовок: <input type="text" name="slider2_title" value="'.$edit['category_config']['slider2_title'].'"><br/>
    Текст: <textarea name="slider2_text" cols="80" rows="2">'.$edit['category_config']['slider2_text'].'</textarea><br/>
    Ссылка: <input type="text" name="slider2_link" value="'.$edit['category_config']['slider2_link'].'"><br/>
    <div id="cropContainerSlider2" style="width:636px; height:401px; position: relative; border:1px solid #ccc;'.(strlen($edit['category_config']['slider2'])>0?' background: url(\''.$edit['category_config']['slider2'].'\') no-repeat center;':'').'"></div><br/>
    <input type="hidden" id="SliderId2" name="slider2" value="'.$edit['category_config']['slider2'].'"/><br/></br>

    Заголовок: <input type="text" name="slider3_title" value="'.$edit['category_config']['slider3_title'].'"><br/>
    Текст: <textarea name="slider3_text" cols="80" rows="2">'.$edit['category_config']['slider3_text'].'</textarea><br/>
    Ссылка: <input type="text" name="slider3_link" value="'.$edit['category_config']['slider3_link'].'"><br/>
    <div id="cropContainerSlider3" style="width:636px; height:401px; position: relative; border:1px solid #ccc;'.(strlen($edit['category_config']['slider3'])>0?' background: url(\''.$edit['category_config']['slider3'].'\') no-repeat center;':'').'"></div><br/>
    <input type="hidden" id="SliderId3" name="slider3" value="'.$edit['category_config']['slider3'].'"/><br/></br>

    Заголовок: <input type="text" name="slider4_title" value="'.$edit['category_config']['slider4_title'].'"><br/>
    Текст: <textarea name="slider4_text" cols="80" rows="2">'.$edit['category_config']['slider4_text'].'</textarea><br/>
    Ссылка: <input type="text" name="slider4_link" value="'.$edit['category_config']['slider4_link'].'"><br/>
    <div id="cropContainerSlider4" style="width:636px; height:401px; position: relative; border:1px solid #ccc;'.(strlen($edit['category_config']['slider4'])>0?' background: url(\''.$edit['category_config']['slider4'].'\') no-repeat center;':'').'"></div><br/>
    <input type="hidden" id="SliderId4" name="slider4" value="'.$edit['category_config']['slider4'].'"/><br/></br>

    Заголовок: <input type="text" name="slider5_title" value="'.$edit['category_config']['slider5_title'].'"><br/>
    Текст: <textarea name="slider5_text" cols="80" rows="2">'.$edit['category_config']['slider5_text'].'</textarea><br/>
    Ссылка: <input type="text" name="slider5_link" value="'.$edit['category_config']['slider5_link'].'"><br/>
    <div id="cropContainerSlider5" style="width:636px; height:401px; position: relative; border:1px solid #ccc;'.(strlen($edit['category_config']['slider5'])>0?' background: url(\''.$edit['category_config']['slider5'].'\') no-repeat center;':'').'"></div><br/>
    <input type="hidden" id="SliderId5" name="slider5" value="'.$edit['category_config']['slider5'].'"/><br/></br>

    <h3>ЦИФРЫ И ФАКТЫ</h3>
    Число: <input type="text" name="number_title" value="'.$edit['category_config']['number_title'].'"><br/>
    Текст: <textarea name="number_text" cols="80" rows="2">'.$edit['category_config']['number_text'].'</textarea><br/>
    Ссылка: <input type="text" name="number_link" value="'.$edit['category_config']['number_link'].'"><br/>

    <h3>КАЛЕНДАРЬ</h3>
    Число: <input type="text" name="date_title" value="'.$edit['category_config']['date_title'].'"><br/>
    Текст: <textarea name="date_text" cols="80" rows="2">'.$edit['category_config']['date_text'].'</textarea><br/>
    Ссылка: <input type="text" name="date_link" value="'.$edit['category_config']['date_link'].'"><br/>

    <h3>БАННЕР</h3>
    Ссылка: <input type="text" name="banner_link" value="'.$edit['category_config']['banner_link'].'"><br/>
    <div id="cropContainerBanner" style="width:308px; height:508px; position: relative; border:1px solid #ccc;'.(strlen($edit['category_config']['banner'])>0?' background: url(\''.$edit['category_config']['banner'].'\') no-repeat center;':'').'"></div><br/>
    <input type="hidden" id="BannerId" name="banner" value="'.$edit['category_config']['banner'].'"/><br/></br>

    <input type="submit" value="'.(intval($_GET['edit_category'])>0?'Сохранить':'Добавить').'"/><br>
    </form>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>№</th>
          <th>Заголовок</th>
          <th>Меню</th>
          <th>Действия</th>
        </tr>
      </thead>
      <tbody>';
      $qnews=$db->query('SELECT * FROM ecrm_category WHERE category_parent='.intval($_GET['category_id']).' ORDER BY category_position ASC');
      while ($new=$db->fetch($qnews))
      {
        echo '<tr>
        <td>
         '.$new['category_id'].'
        </td>
        <td>
         '.$new['category_name'].'
        </td>
        <td>
         '.$catname[$new['category_type']].'
        </td>
        <td>
         <!--<a href="?up_id='.$new['category_id'].'">Вверх</a>
         <a href="?down_id='.$new['category_id'].'">Вниз</a>-->
         <a href="?edit_category='.$new['category_id'].'">Редактировать</a>
         <!--<a href="?del_category_id='.$new['category_id'].'">Удалить</a>-->
        </td>
      </tr>';
      }
      echo '
      </tbody>
    </div>';
    $script='    var croppicContainerEyecandyOptions = {
        uploadUrl:\'img_save_to_file.php\',
        cropUrl:\'img_crop_to_file.php\',
        imgEyecandy:true,
        outputUrlId:\'myOutputId\'
    }

    var cropContainerEyecandy = new Croppic(\'cropContainerEyecandy\', croppicContainerEyecandyOptions);


    var croppicContainerEyecandyOptions1 = {
        uploadUrl:\'img_save_to_file.php\',
        cropUrl:\'img_crop_to_file.php\',
        imgEyecandy:true,
        outputUrlId:\'SliderId1\'
    }

    var cropContainerEyecandy1 = new Croppic(\'cropContainerSlider1\', croppicContainerEyecandyOptions1);

    var croppicContainerEyecandyOptions2 = {
        uploadUrl:\'img_save_to_file.php\',
        cropUrl:\'img_crop_to_file.php\',
        imgEyecandy:true,
        outputUrlId:\'SliderId2\'
    }

    var cropContainerEyecandy2 = new Croppic(\'cropContainerSlider2\', croppicContainerEyecandyOptions2);

    var croppicContainerEyecandyOptions3 = {
        uploadUrl:\'img_save_to_file.php\',
        cropUrl:\'img_crop_to_file.php\',
        imgEyecandy:true,
        outputUrlId:\'SliderId3\'
    }

    var cropContainerEyecandy3 = new Croppic(\'cropContainerSlider3\', croppicContainerEyecandyOptions3);

    var croppicContainerEyecandyOptions4 = {
        uploadUrl:\'img_save_to_file.php\',
        cropUrl:\'img_crop_to_file.php\',
        imgEyecandy:true,
        outputUrlId:\'SliderId4\'
    }

    var cropContainerEyecandy4 = new Croppic(\'cropContainerSlider4\', croppicContainerEyecandyOptions4);

    var croppicContainerEyecandyOptions5 = {
        uploadUrl:\'img_save_to_file.php\',
        cropUrl:\'img_crop_to_file.php\',
        imgEyecandy:true,
        outputUrlId:\'SliderId5\'
    }

    var cropContainerEyecandy5 = new Croppic(\'cropContainerSlider5\', croppicContainerEyecandyOptions5);

    var croppicContainerEyecandyOptionsBanner = {
        uploadUrl:\'img_save_to_file.php\',
        cropUrl:\'img_crop_to_file.php\',
        imgEyecandy:true,
        outputUrlId:\'BannerId\'
    }

    var cropContainerEyecandyBanner = new Croppic(\'cropContainerBanner\', croppicContainerEyecandyOptionsBanner);
';
/*
if ($_POST['act']=='add_company')
{
    if ((strtotime($_POST['company_start'])<strtotime($_POST['company_end']))&&(trim($_POST['company_name'])!=''))
    {
    	$qadd_company=$db->query('INSERT INTO ecrm_companies (company_name,company_start,company_end,company_host,company_vk_id) VALUES (\''.addslashes($_POST['company_name']).'\','.strtotime($_POST['company_start']).','.strtotime($_POST['company_end']).',\''.addslashes($_POST['company_host']).'\',\''.addslashes($_POST['company_vk_id']).'\')');
    	$id_add_company=$db->insert_id($qadd_company);
    	$qadd_uc=$db->query('INSERT INTO ecrm_uc (user_id,company_id) VALUES ('.$user['user_id'].','.$id_add_company.')');
        $qreverse[]='DELETE FROM ecrm_companies WHERE company_id='.$id_add_company;
        $qreverse[]='DELETE FROM ecrm_uc WHERE uc_id='.$db->insert_id($qadd_uc);
        $db->query('INSERT INTO `ecrm.ecrm_user_logs` (log_time,log_reverse_sql,user_id) VALUES ('.time().',\''.addslashes(json_encode($qreverse)).'\','.$user['user_id'].')');
    }
    else
    {
        echo '<script>alert(\'Дата начала больше даты конца! / Название комании пустое!\');</script>';
    }
}
elseif ($_POST['act']=='edit_company')
{
    if ((strtotime($_POST['company_start'])<strtotime($_POST['company_end']))&&(trim($_POST['company_name'])!=''))
    {
    	$qedit_company=$db->query('SELECT * FROM ecrm_companies WHERE company_id='.$_GET['company_id']);
    	$edit_company=$db->fetch($qedit_company);
    	foreach ($edit_company as $key => $value)
    	{
    	    $params.=$zap.$key.'=\''.addslashes($value).'\'';
            $zap=',';
    	}
    	// print_r($_POST);
    	$q=$db->query('UPDATE ecrm_companies SET company_name=\''.addslashes($_POST['company_name']).'\', company_start='.strtotime($_POST['company_start']).' ,company_end='.strtotime($_POST['company_end']).' ,company_host=\''.addslashes($_POST['company_host']).'\' ,company_vk_id=\''.addslashes($_POST['company_vk_id']).'\' WHERE company_id='.$_GET['company_id']);
    	$qreverse[]='UPDATE ecrm_companies SET '.$params.'WHERE company_id='.$_GET['company_id'];
        $db->query('INSERT INTO `ecrm.ecrm_user_logs` (log_time,log_reverse_sql,user_id) VALUES ('.time().',\''.addslashes(json_encode($qreverse)).'\','.$user['user_id'].')');
    }
    else
    {
        echo '<script>alert(\'Дата начала больше даты конца! / Название комании пустое!\');</script>';
    }
}
elseif ($_GET['del_company_id']!='')
{
	$qdel_company=$db->query('SELECT * FROM ecrm_companies WHERE company_id='.$_GET['del_company_id']);
	$del_company=$db->fetch($qdel_company);
	// print_r($del_company);
	foreach ($del_company as $key => $item)
	{
        $params.=$paramzap.$key;
        $paramzap=',';
        $values.=$valuezap.'\''.addslashes($item).'\'';
        $valuezap=',';
	}
	$qreverse[]='INSERT INTO ecrm_companies ('.$params.') VALUES ('.$values.')';
	// echo 'SELECT * FROM ecrm_uc WHERE company_id='.$_GET['del_company_id'].' AND user_id='.$user['user_id'];
	$qdel_uc=$db->query('SELECT * FROM ecrm_uc WHERE company_id='.$_GET['del_company_id'].' AND user_id='.$user['user_id']);
	$del_uc=$db->fetch($qdel_uc);
    $params='';
    $paramzap='';
    $values='';
    $valuezap='';
	foreach ($del_uc as $key => $item)
	{
        $params.=$paramzap.$key;
        $paramzap=',';
        $values.=$valuezap.'\''.addslashes($item).'\'';
        $valuezap=',';
	}
	$qreverse[]='INSERT INTO ecrm_uc ('.$params.') VALUES ('.$values.')';

	$qdel=$db->query('DELETE FROM ecrm_companies WHERE company_id='.$_GET['del_company_id']);
	$qdel=$db->query('DELETE FROM ecrm_uc WHERE company_id='.$_GET['del_company_id'].' AND user_id='.$user['user_id']);
	$db->query('INSERT INTO `ecrm.ecrm_user_logs` (log_time,log_reverse_sql,user_id) VALUES ('.time().',\''.addslashes(json_encode($qreverse)).'\','.$user['user_id'].')');
}
elseif ($_GET['start_company_id']!='')
{
    $qactive=$db->query('SELECT * FROM ecrm_companies WHERE company_id='.$_GET['start_company_id'].' LIMIT 1');
    $active_comp=$db->fetch($qactive);
    if ($active_comp['company_active']==0) $db->query('UPDATE ecrm_companies SET company_active=1 WHERE company_id='.$_GET['start_company_id']);
    else $db->query('UPDATE ecrm_companies SET company_active=0 WHERE company_id='.$_GET['start_company_id']);
}
elseif ($_GET['del_au_id']!='')
{
    $db->query('DELETE FROM ecrm_active_users WHERE company_id='.intval($_GET['del_au_id']));
}

$qcampanies=$db->query('SELECT * FROM ecrm_users as a LEFT JOIN ecrm_uc as b ON a.user_id=b.user_id LEFT JOIN ecrm_companies as c ON b.company_id=c.company_id WHERE a.user_id='.$user['user_id']);

echo '
    <div class="container">
    <ul class="breadcrumb">
	  <li class="active">Главная</li>
	</ul>
    <legend>Список кампаний</legend>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>Название</th>
          <th>Начало кампании</th>
          <th>Конец кампании</th>
          <th>Действия</th>
        </tr>
      </thead>
      <tbody>
      ';
while ($campany=$db->fetch($qcampanies))
{
	if (($_GET['company_id']!='')&&($_GET['company_id']==$campany['company_id'])) $company_edit=$campany;
    if (intval($campany['company_id'])>0) 
        {
            echo '
    <tr>
        <td>'.$campany['company_id'].'</td>
        <td>'.$campany['company_name'].'</td>
        <td>'.date('d.m.Y',$campany['company_start']).'</td>
        <td>'.date('d.m.Y',$campany['company_end']).'</td>
        <td>
                <a href="?start_company_id='.$campany['company_id'].'"><button class="btn btn-small '.($campany['company_active']==1?'btn-success':'btn-danger').'"><i class="icon-off"></i></button></a>
                <div class="btn-group">
                  <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">
                    Настройки
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="/admin/plans/?company_id='.$campany['company_id'].'">Расписание</a></li>
                    <li><a href="/admin/rules/?company_id='.$campany['company_id'].'">Стратегия</a></li>
                    <li><a href="/admin/target/?company_id='.$campany['company_id'].'">Целевые аудитории</a></li>
                    <li><a href="/admin/targetaction/?company_id='.$campany['company_id'].'">Целевые действия</a></li>
                    <li><a href="/admin/channels/?company_id='.$campany['company_id'].'">Каналы коммуникации</a></li>
                    <li><a href="/admin/users/?company_id='.$campany['company_id'].'">Активные участники</a></li>
                    <li><a href="/admin/lead/?company_id='.$campany['company_id'].'">Лиды</a></li>
                    <li><a href="/admin/stats/?company_id='.$campany['company_id'].'">Статистика по компании</a></li>
                    <li><a href="http://ecrm/import/upload.php?company_id='.$campany['company_id'].'">Импорт активных участников</a></li>
                  </ul>
                </div>
                <a href="/admin/company/export_audience.php?export_audience_company_id='.$campany['company_id'].'"><button class="btn btn-small btn-success"><i class="icon-arrow-down"></i></button></a>
                <a href="?del_au_id='.$campany['company_id'].'"><button class="btn btn-small btn-success">Очистить АУ</button></a>
                <a href="?company_id='.$campany['company_id'].'"><button class="btn btn-small btn-success"><i class="icon-pencil"></i></button></a>
                <a href="?del_company_id='.$campany['company_id'].'" onclick="return confirm(\'Удалить кампанию?\');"><button class="btn btn-small btn-danger" type="button"><i class="icon-remove"></i></button></a>
        </td>
    </tr>
    ';    
        }
}
echo '</tbody></table>';

// if ($_GET['company_id']!='')
{
	echo '<form method="POST" action="?company_id='.$_GET['company_id'].'">
	<input type="hidden" name="act" value="'.($_GET['company_id']!=''?'edit':'add').'_company">
	Название кампании: <input type="text" name="company_name" value="'.$company_edit['company_name'].'"><br>
	Начало периода: <input type="text" name="company_start" value="'.($company_edit['company_start']!=''?date('d-m-Y',$company_edit['company_start']):date('d').'-'.date('m').'-'.date('Y')).'" id="dp" data-date="'.($company_edit['company_start']!=''?date('d-m-Y',$company_edit['company_start']):date('d').'-'.date('m').'-'.date('Y')).'" data-date-format="dd-mm-yyyy"><br>
	Конец периода: <input type="text" name="company_end" value="'.($company_edit['company_end']!=''?date('d-m-Y',$company_edit['company_end']):date('d').'-'.date('m').'-'.date('Y')).'" id="dp1" data-date="'.($company_edit['company_end']!=''?date('d-m-Y',$company_edit['company_end']):date('d').'-'.date('m').'-'.date('Y')).'" data-date-format="dd-mm-yyyy"><br>
    Хост компании: <input type="text" name="company_host" value="'.$company_edit['company_host'].'"><br>
    ID группы вконтакте: <input type="text" name="company_vk_id" value="'.$company_edit['company_vk_id'].'"><br>
	<br><button type="submit" class="btn">'.($_GET['company_id']!=''?'Редактировать':'Добавить').'</button>
	</form>';
}

$script='$(\'#dp\').datepicker();$(\'#dp1\').datepicker();';
*/
echo get_footer('../',$script);

?>