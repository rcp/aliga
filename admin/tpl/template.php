<?

function get_header($path)
{
  global $user;
	$out='<!DOCTYPE html>
<html lang="en">
  <head>
  	<script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <meta charset="utf-8">
    <title>А ЛИГА &copy; Журнал о спорте и жизни. NBA, NHL, NFL, MLB, EXTREME, MMA, WORKOUT.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="'.$path.'css/bootstrap.css" rel="stylesheet">
    <link href="'.$path.'css/datepicker.css" rel="stylesheet">
    <link href="'.$path.'css/bootstrap-timepicker.css" rel="stylesheet">
    <link href="'.$path.'css/tagmanager.css" rel="stylesheet"/>
    <script src="//cdn.jsdelivr.net/typeahead.js/0.9.3/typeahead.min.js"></script>
    <link href="'.$path.'css/uploadfile.css" rel="stylesheet">
    <link href="'.$path.'css/croppic.css" rel="stylesheet">
    <script src="'.$path.'js/tagmanager.js"></script>

    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="'.$path.'css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="'.$path.'js/html5shiv.js"></script>
    <![endif]-->

  </head>

  <body>
	<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
	<div class="container">
	  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	    <span class="icon-bar"></span>
	  </button>
	  <a class="brand" href="/admin/company" style="background: url(\''.$path.'img/logo.png\') center no-repeat;" title="Алига">&nbsp;</a>
	  <div class="nav-collapse collapse">
	    <ul class="nav">
	      <li><a href="'.$path.'news/">Новости</a></li>
        <li><a href="'.$path.'stuff/">Материалы</a></li>
        <li><a href="'.$path.'rubrika/">Рубрики</a></li>
        <li><a href="'.$path.'category">Разделы</a></li>
        <li><a href="'.$path.'sources">Источники</a></li>
	    </ul>
      <ul class="nav pull-right">
        <li><a href="#"><i class="icon-user"></i> '.$user['user_name'].'</a></li>
        <li><a href="'.$path.'logout.php"><i class="icon-off"></i></a></li>
      </ul>
	  </div><!--/.nav-collapse -->
	</div>
	</div>
	</div>
';
  	return $out;
}

function get_footer($path,$script)
{
	$out='    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="'.$path.'js/bootstrap-transition.js"></script>
    <script src="'.$path.'js/bootstrap-alert.js"></script>
    <script src="'.$path.'js/bootstrap-modal.js"></script>
    <script src="'.$path.'js/bootstrap-dropdown.js"></script>
    <script src="'.$path.'js/bootstrap-scrollspy.js"></script>
    <script src="'.$path.'js/bootstrap-tab.js"></script>
    <script src="'.$path.'js/bootstrap-tooltip.js"></script>
    <script src="'.$path.'js/bootstrap-popover.js"></script>
    <script src="'.$path.'js/bootstrap-button.js"></script>
    <script src="'.$path.'js/bootstrap-collapse.js"></script>
    <script src="'.$path.'js/bootstrap-carousel.js"></script>
    <script src="'.$path.'js/bootstrap-typeahead.js"></script>
    <script src="'.$path.'js/bootstrap-datepicker.js"></script>
    <script src="'.$path.'js/bootstrap-timepicker.js"></script>
    <script src="'.$path.'js/jsoneditor.js"></script>
    <script src="'.$path.'js/jquery.uploadfile.js"></script>
    <script src="'.$path.'js/bootstrap.file-input.js"></script>
    <script src="'.$path.'js/croppic.js"></script>
    <script>
        '.$script.'
    </script>

  </body>
</html>';
	return $out;
}

?>