<?

require_once('../com/db.php');
require_once('../com/config.php');
require_once('../login.php');
require_once('../tpl/template.php');

$db=new database();
$db->connect();
//print_r($_POST['hidden-stuff_tags']);
// die();
login();

// if (file_exists('../img_uploads/'.intval($_GET['stuff_id']).'.jpg')) $title_img=$config['html_root'].'/img_uploads/'.intval($_GET['stuff_id']).'.jpg';
// else $title_img=$config['html_root'].'/img_uploads/0.jpg';

if (intval($_POST['stuff_id'])>0)
{
  $db->query('UPDATE ecrm_stuff SET stuff_title=\''.addslashes($_POST['stuff_title']).'\', stuff_tags=\''.addslashes($_POST['hidden-stuff_tags']).'\', category_id=\''.intval($_POST['category_id']).'\', stuff_content=\''.addslashes($_POST['stuff_content']).'\' WHERE stuff_id='.intval($_POST['stuff_id']));
  die();
}

$qnews=$db->query('SELECT * FROM ecrm_stuff WHERE stuff_id='.intval($_GET['stuff_id']).' LIMIT 1');
$new=$db->fetch($qnews);

if ($new['stuff_img']!=='')
{
  $title_img=$new['stuff_img'];
}
else
{
  $title_img='admin/img_uploads/0.jpg';
}

$categories=array();
$qcat=$db->query('SELECT * FROM ecrm_category WHERE category_parent=0 and category_id>1');
while ($cat=$db->fetch($qcat)) {
  $prefix='';
  // if (intval($cat['category_type'])==1) $prefix='ИГРА --> ';
  $categories[]=array('id'=>$cat['category_id'],'name'=>$cat['category_name']);
  $qcat2=$db->query('SELECT * FROM ecrm_category WHERE category_parent='.intval($cat['category_id']));
  while ($cat2=$db->fetch($qcat2)) {
    $categories[]=array('id'=>$cat2['category_id'],'name'=>$cat2['category_name']);
  }
}

$qcat1=$db->query('SELECT * FROM ecrm_category WHERE category_parent=0 ORDER BY category_position');
while ($cat1=$db->fetch($qcat1)) {
  $categories_menu[intval($cat1['category_type'])][]=$cat1;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="../../css/style.css" />
  <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
  <script src="../../js/jquery.bxslider.js" type="text/javascript"></script>
  <script src="../../js/myScript.js" type="text/javascript"></script>
  <script src="../../js/ckeditor/ckeditor.js"></script>
  <link href="../css/tagmanager.css" rel="stylesheet"/>
  <script src="//cdn.jsdelivr.net/typeahead.js/0.9.3/typeahead.min.js"></script>
  <script src="../js/tagmanager.js"></script>
  <link href="../css/croppic.css" rel="stylesheet">
  <script src="../js/croppic.js"></script>
  <script src="../../js/SimpleAjaxUploader.min.js"></script>
  <BASE href="/"/>
<title>А ЛИГА &copy; <?=$new['stuff_head_title'];?></title>
</head>

<body>
<div class="wrapper-b">
  <div class="header-b relative-b clearfix">
    <a class="logo-b" href="#">a-liga</a>
    <div class="row">
      <div class="col-8">
        <div class="ml-95">
          <div class="quote-b">
            <?
              foreach ($categories_menu[0] as $key => $cat) {
                echo '<span>'.$cat['category_name'].'</span> ';
              }
            ?></div>
          <ul class="header-nav-b clearfix">
            <?
              foreach ($categories_menu[1] as $key => $cat) {
                echo '<li><a href="#">'.$cat['category_name'].'</a></li> ';
              }
              ?>
          </ul>
        </div>
      </div>
      <div class="col-4">
        <form class="search-header-b bd-7-yellow pull-right" action="">
          <input class="pull-right" type="text" name="" id="" placeholder=""/>
          <input type="submit" value="send" />
        </form>
      </div>
    </div>
    <div class="line-header-gray pull-right"></div>
  </div><!-- header-b -->
  
  <div class="content-b">
    <form id="inside-form" method="post">
    <div class="row container">
      <div class="col-8">
        <div class="main-video-b">
          <div class="name-b" id="titleeditor" contenteditable="true"><?=(strlen($new['stuff_title'])>0?$new['stuff_title']:'Заголовок');?></div>
          <div id="insideeditor" contenteditable="true">
            <?=(strlen($new['stuff_content'])>0?$new['stuff_content']:'<iframe width="636" height="360" src="http://www.youtube.com/embed/2JKeWGz-ZxQ" frameborder="0" allowfullscreen></iframe>');?>
          </div>
          <div class="params-b clearfix">
            <div class="type-b"> 
            <select name="category_id">
          <?
            foreach ($categories as $key => $value) {
              echo '<option value="'.$value['id'].'" '.(($new['category_id']==$value['id'])?'selected':'').'>'.$value['name'].'</option>';
            }
          ?>
          </select>
            </div>
            <div class="like-b"><a href="#"><i></i> <span>0</a></span></div>
            <div class="view-b"><a href="#"><i></i> <span>0</a></span></div>
            <div class="date-b"><a href="#"><?=date('d.m',$new['stuff_date']);?></a></div>
            <input type="hidden" name="stuff_id" id="stuff_id" value="<?=$new['stuff_id'];?>"/>
        <input type="hidden" name="stuff_title" id="stuff_title"/>
        <input type="hidden" name="stuff_content" id="stuff_content"/>
        <a href="#" id="submit-inside" style="border: 3px solid black; background-color: white; position: fixed; left: 50px; top: 30px; color: black; font-weight: bold; padding: 20px; opacity: 0.8; filter: alpha(opacity=80);" onclick="return false;">СОХРАНИТЬ</a>
        
          </div>
        </div>
        <div class="bd-10-yellow pt-15 pb-15 mt-2 clearfix">
          <ul class="main-social-b ml-25 pull-left">
            <li class="i-fb"><a href="#">fb</a></li>
            <li class="i-vk"><a href="#">vk</a></li>
            <li class="i-twitter"><a href="#">twitter</a></li>
            <li class="i-gp"><a href="#">gp</a></li>
            <li class="i-info"><a href="#">info</a></li>
          </ul>
          <div class="counter-comment-b pull-right mr-45">
            <a class="lh-50" href="#">КОММЕНТАРИИ ( 0 )</a>
          </div>
        </div>
        
        <div class="tags-b ml-40 mt-25 clearfix">
          <i></i>
          <div class="pull-left ml-50">
                        <input type="text" name="stuff_tags" placeholder="Теги" class="tm-input" value=""/>
            </form>
          </div>
        </div>
        
        <div class="title-type1 ml-40 mt-25 clearfix">
          <i></i>
          <h3 class="ml-50">ВАС ТАКЖЕ МОЖЕТ ЗАИНТЕРЕСОВАТЬ: </h3>
        </div>
      </div>
      <div class="col-4">
        <div class="h-45 bd-4-gray"></div>
        <div class="b-h496 bd-4-gray mt-25">
          <a href="#"><img src="img/img-b-4.jpg" alt="img" /></a>
        </div>
      </div>
    </div><!-- article-block -->
    
    <div class="container">
      <div class="wr-video-slider-b">
        <ul class="video-slider-b">
          <li>
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom text-bg-green">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline">БЕЙСБОЛ:</span></li>
                      <li><span>НЕОЖИДАННАЯ КОНЦОВКА МАТЧА</span></li>
                    </ul>
                  </a>
                </div>
                <a class="video-b" href="#"><img src="img/img-item-11.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ВИДЕО</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</a></span></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</a></span></div>
              </div>
            </div>
          </li>
          <li>
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom text-bg-green">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline">XTREME:</span></li>
                      <li><span>ДАЕШЬ ОТКРОВЕННЫЙ СЕРФИНГ</span></li>
                    </ul>
                  </a>
                </div>
                <a class="video-b" href="#"><img src="img/img-item-12.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ВИДЕО</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
              </div>
            </div>
          </li>
          <li>
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline">РЕКЛАМА:</span></li>
                      <li><span>ASKET JINGLE ALL THE WAY !</span></li>
                    </ul>
                  </a>
                </div>
                <a class="video-b" href="#"><img src="img/img-item-13.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ВИДЕО</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
              </div>
            </div>
          </li>
          <li>
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom text-bg-green">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline">XTREME:</span></li>
                      <li><span>ДАЕШЬ ОТКРОВЕННЫЙ СЕРФИНГ</span></li>
                    </ul>
                  </a>
                </div>
                <a class="video-b" href="#"><img src="img/img-item-12.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ВИДЕО</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
              </div>
            </div>
          </li>
        </ul>
        <div class="video-slider-btn-navig clearfix">
          <div id="video-slider-prev"></div>
          <div id="video-slider-next"></div>
        </div>
      </div><!-- wr-statistic-slider-b -->
    </div><!-- container -->
    
    <div class="row container">
      <div class="col-8">
        <div class="row">
          <div class="w-416 ml-10 mr-10 pull-left">
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline text-color-yellow">ПРОСПОРТ:</span></li>
                      <li><span>РЕВОЛЮЦИЯ “4 HOUR BODY” ОТ ТИМОТИ ФЭРРИСА</span></li>
                      <li><span>СЖИГАЕТ МИЛЛИОНЫ ПРОБЛЕМ</span></li>
                    </ul>
                  </a>
                </div>
                <a href="#"><img src="img/img-item-17.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ТРЕНИРОВКА</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
              </div>
            </div>
          </div>
          <div class="w-200 ml-10 mr-10 pull-left">
            <div class="main-item-view2">
              <div class="img-b"> <a href="#"><img src="img/img-item-circle-1.png" alt="img" /></a></div>
              <div class="text-b">
                <div class="subject-b"> <a class="text-underline" href="#">НАСТРОЙ</a> </div>
                <p>Первородная мотивация от секс символов современности</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="main-item-view">
          <div class="img-b">
            <div class="text-b text-bottom">
              <a href="#">
                <ul>
                  <li><span class="text-underline text-color-yellow">ПО МЕСТАМ:</span></li>
                  <li><span>ИСТОРИЯ ВЕЛИКОГО СТАДИОНА</span></li>
                  <li><span>СЕНТ-ЛУИС</span></li>
                </ul>
              </a>
            </div>
            <a class="video-b" href="#"><img src="img/img-item-15.jpg" alt="img" /></a>
          </div>
          <div class="params-b">
            <div class="name-b"><a href="#">ТАЙМ - АУТ</a></div>
            <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
            <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
            <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
          </div>
        </div>
      </div>
    </div><!-- row container -->
  </div><!-- content -->
</div><!-- wrapper-b -->
            <script>
$(function () {

    $(".tm-input").tagsManager({prefilled: ["<?
      echo implode('","',explode(',', $new['stuff_tags']));
      ?>"]});


                CKEDITOR.disableAutoInline = true;
                CKEDITOR.inline( 'insideeditor', {
    language: 'ru',
    uiColor: '#FFFFFF',
    extraPlugins: 'sourcedialog,doksoft_gallery,tableresize,tabletools,blockquote',
    removePlugins: 'sourcearea'
} );            

                CKEDITOR.inline( 'titleeditor', {
    language: 'ru',
    uiColor: '#FFFFFF',
    extraPlugins: 'sourcedialog',
    removePlugins: 'sourcearea'
} );    
    

function sendform()
{
  var data = CKEDITOR.instances.insideeditor.getData();
  $('#stuff_content').val(data);
  var data2 = CKEDITOR.instances.titleeditor.getData();
  $('#stuff_title').val(data2);
  var jqxhr = $.post( "", $( "#inside-form" ).serialize(), function() {
  alert( "Страница сохранена" );
}).fail(function() {
    alert( "Ошибка сохранения" );
  });
}
$( "#submit-inside" ).click(function() {
  sendform();
});

});

            </script>
</body>
</html>


<?


/*
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="../../css/style.css" />
  <script src="../../js/jquery-1.9.1.min.js" type="text/javascript"></script>
  <script src="../../js/jquery.bxslider.js" type="text/javascript"></script>
  <script src="../../js/myScript.js" type="text/javascript"></script>
  <script src="../../js/ckeditor/ckeditor.js"></script>
  <link href="../css/tagmanager.css" rel="stylesheet"/>
  <script src="//cdn.jsdelivr.net/typeahead.js/0.9.3/typeahead.min.js"></script>
  <script src="../js/tagmanager.js"></script>
  <link href="../css/croppic.css" rel="stylesheet">
  <script src="../js/croppic.js"></script>
  <script src="../../js/SimpleAjaxUploader.min.js"></script>
  <BASE href="/"/>
<title>А ЛИГА &copy; <?=$new['stuff_head_title'];?></title>
</head>

<body>
<div class="wrapper-b">
  <div class="header-b relative-b clearfix">
    <a class="logo-b" href="#">a-liga</a>
    <div class="row">
      <div class="col-8">
        <div class="ml-95">
          <div class="quote-b">
            <?
              foreach ($categories_menu[0] as $key => $cat) {
                echo '<span>'.$cat['category_name'].'</span> ';
              }
            ?></div>
          <ul class="header-nav-b clearfix">
            <?
              foreach ($categories_menu[1] as $key => $cat) {
                echo '<li><a href="#">'.$cat['category_name'].'</a></li> ';
              }
              ?>
          </ul>
        </div>
      </div>
      <div class="col-4">
        <form class="search-header-b bd-7-yellow pull-right" action="">
          <input class="pull-right" type="text" name="" id="" placeholder=""/>
          <input type="submit" value="send" />
        </form>
      </div>
    </div>
    <div class="line-header-gray pull-right"></div>
  </div><!-- header-b -->
  
  <div class="content-b">
    <form id="inside-form" method="post">
    <div class="container info-block-type1" id="title_img" style="background: url('<?=$title_img;?>') no-repeat center;">
      <div class="text-b">
        <div id="titleeditor" class="subject-b clearfix" contenteditable="true">
          <?=(strlen($new['stuff_title'])>0?$new['stuff_title']:'Заголовок');?>
        </div>
        <!--<div style="border: 3px solid black; background-color: white; position: absolute; left: 700px; top: 5px; color: black; font-weight: bold; padding: 20px; opacity: 0.8; filter: alpha(opacity=80);">
          <input type="button" id="uploadButton" value="Загрузить изображение"/>
          <p id="sizeBox"></p>
          <p id="progress"></p>
        </div>-->
        <ul class="params-b clearfix">
          <li class="i-time">
            <i></i>
            <p><?=date('d.m',$new['stuff_date']);?></p>
          </li>
          <li class="i-comm">
            <i></i>
            <p>0</p>
          </li>
          <li class="i-like">
            <a href="#">
              <i></i>
              <p>0</p>
            </a>
          </li>
          
        </ul>
        
        <div class="breadcrumbs-b">
                    <select name="category_id">
          <?
            foreach ($categories as $key => $value) {
              echo '<option value="'.$value['id'].'" '.(($new['category_id']==$value['id'])?'selected':'').'>'.$value['name'].'</option>';
            }
          ?>
          </select>
        </div>
      </div>
      <!--<img src="" id="title-img" alt="img" />-->
    </div><!-- info-block-type1 -->
    
    <div class="row container article-block">
      <div class="col-8">
        <div id="insideeditor" contenteditable="true">
        <?
          echo $new['stuff_content'];
        ?>
        </div>
    <br>
    <br>

        <input type="hidden" name="stuff_id" id="stuff_id" value="<?=$new['stuff_id'];?>"/>
        <input type="hidden" name="stuff_title" id="stuff_title"/>
        <input type="hidden" name="stuff_img" id="myOutputId" value="<?=$new['stuff_img'];?>"/>
        <input type="hidden" name="stuff_content" id="stuff_content"/>
        <a href="#" id="submit-inside" style="border: 3px solid black; background-color: white; position: fixed; left: 50px; top: 30px; color: black; font-weight: bold; padding: 20px; opacity: 0.8; filter: alpha(opacity=80);" onclick="return false;">СОХРАНИТЬ</a>
        <div class="bd-10-yellow pt-15 pb-15 clearfix">
          <ul class="main-social-b ml-25 pull-left">
            <li class="i-fb"><a href="#">fb</a></li>
            <li class="i-vk"><a href="#">vk</a></li>
            <li class="i-twitter"><a href="#">twitter</a></li>
            <li class="i-gp"><a href="#">gp</a></li>
            <li class="i-info"><a href="#">info</a></li>
          </ul>
          <div class="counter-comment-b pull-right mr-45">
            <a class="lh-50" href="#">КОММЕНТАРИИ ( 0 )</a>
          </div>
        </div>
        
        <div class="tags-b ml-40 mt-25 clearfix">
          <i></i>
          <div class="pull-left ml-50">
            <input type="text" name="stuff_tags" placeholder="Теги" class="tm-input" value=""/>
            </form>
          </div>
        </div>
        
        <div class="title-type1 ml-40 mt-25 clearfix">
          <i></i>
          <h3 class="ml-50">ВАС ТАКЖЕ МОЖЕТ ЗАИНТЕРЕСОВАТЬ: </h3>
        </div>
      </div>
      <div class="col-4">
        <div class="b-h495 bd-4-gray">
          <a href="#"><img src="img/img-b-4.jpg" alt="img" /></a>
        </div>
      </div>
    </div><!-- article-block -->
    
    <div class="container">
      <div class="wr-video-slider-b">
        <ul class="video-slider-b">
          <li>
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom text-bg-green">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline">БЕЙСБОЛ:</span></li>
                      <li><span>НЕОЖИДАННАЯ КОНЦОВКА МАТЧА</span></li>
                    </ul>
                  </a>
                </div>
                <a class="video-b" href="#"><img src="img/img-item-11.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ВИДЕО</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</a></span></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</a></span></div>
              </div>
            </div>
          </li>
          <li>
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom text-bg-green">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline">XTREME:</span></li>
                      <li><span>ДАЕШЬ ОТКРОВЕННЫЙ СЕРФИНГ</span></li>
                    </ul>
                  </a>
                </div>
                <a class="video-b" href="#"><img src="img/img-item-12.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ВИДЕО</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
              </div>
            </div>
          </li>
          <li>
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline">РЕКЛАМА:</span></li>
                      <li><span>ASKET JINGLE ALL THE WAY !</span></li>
                    </ul>
                  </a>
                </div>
                <a class="video-b" href="#"><img src="img/img-item-13.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ВИДЕО</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
              </div>
            </div>
          </li>
          <li>
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom text-bg-green">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline">XTREME:</span></li>
                      <li><span>ДАЕШЬ ОТКРОВЕННЫЙ СЕРФИНГ</span></li>
                    </ul>
                  </a>
                </div>
                <a class="video-b" href="#"><img src="img/img-item-12.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ВИДЕО</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
              </div>
            </div>
          </li>
        </ul>
        <div class="video-slider-btn-navig clearfix">
          <div id="video-slider-prev"></div>
          <div id="video-slider-next"></div>
        </div>
      </div><!-- wr-statistic-slider-b -->
    </div><!-- container -->
    
    <div class="row container">
      <div class="col-8">
        <div class="row">
          <div class="w-416 ml-10 mr-10 pull-left">
            <div class="main-item-view">
              <div class="img-b">
                <div class="text-b text-bottom">
                  <a href="#">
                    <ul>
                      <li><span class="text-underline text-color-yellow">ПРОСПОРТ:</span></li>
                      <li><span>РЕВОЛЮЦИЯ “4 HOUR BODY” ОТ ТИМОТИ ФЭРРИСА</span></li>
                      <li><span>СЖИГАЕТ МИЛЛИОНЫ ПРОБЛЕМ</span></li>
                    </ul>
                  </a>
                </div>
                <a href="#"><img src="img/img-item-17.jpg" alt="img" /></a>
              </div>
              <div class="params-b">
                <div class="name-b"><a href="#">ТРЕНИРОВКА</a></div>
                <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
                <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
                <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
              </div>
            </div>
          </div>
          <div class="w-200 ml-10 mr-10 pull-left">
            <div class="main-item-view2">
              <div class="img-b"> <a href="#"><img src="img/img-item-circle-1.png" alt="img" /></a></div>
              <div class="text-b">
                <div class="subject-b"> <a class="text-underline" href="#">НАСТРОЙ</a> </div>
                <p>Первородная мотивация от секс символов современности</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-4">
        <div class="main-item-view">
          <div class="img-b">
            <div class="text-b text-bottom">
              <a href="#">
                <ul>
                  <li><span class="text-underline text-color-yellow">ПО МЕСТАМ:</span></li>
                  <li><span>ИСТОРИЯ ВЕЛИКОГО СТАДИОНА</span></li>
                  <li><span>СЕНТ-ЛУИС</span></li>
                </ul>
              </a>
            </div>
            <a class="video-b" href="#"><img src="img/img-item-15.jpg" alt="img" /></a>
          </div>
          <div class="params-b">
            <div class="name-b"><a href="#">ТАЙМ - АУТ</a></div>
            <div class="date-b"><a href="#">19 ФЕВРАЛЯ</a></div>
            <div class="like-b"><a href="#"><i></i> <span>913</span></a></div>
            <div class="view-b"><a href="#"><i></i> <span>1672</span></a></div>
          </div>
        </div>
      </div>
    </div><!-- row container -->
  </div><!-- content -->
</div><!-- wrapper-b -->
            <script>
$(function () {

    $(".tm-input").tagsManager({prefilled: ["<?
      echo implode('","',explode(',', $new['stuff_tags']));
      ?>"]});


                CKEDITOR.disableAutoInline = true;
                CKEDITOR.inline( 'insideeditor', {
    language: 'ru',
    uiColor: '#FFFFFF',
    extraPlugins: 'sourcedialog,doksoft_gallery,tableresize,tabletools,blockquote',
    removePlugins: 'sourcearea'
} );            

                CKEDITOR.inline( 'titleeditor', {
    language: 'ru',
    uiColor: '#FFFFFF',
    extraPlugins: 'sourcedialog',
    removePlugins: 'sourcearea'
} );    
    
var sizeBox = document.getElementById('sizeBox'), // container for file size info
    progress = document.getElementById('progress'); // the element we're using for a progress bar

    var croppicContainerEyecandyOptions = {
        uploadUrl:'admin/stuff/img_save_to_file-title.php',
        cropUrl:'admin/stuff/img_crop_to_file-title.php',
        imgEyecandy:false,
        outputUrlId:'myOutputId'
    }

    var cropContainerEyecandy = new Croppic('title_img', croppicContainerEyecandyOptions);


// var uploader = new ss.SimpleUpload({
//       button: 'uploadButton', // file upload button
//       url: '<?=$config['html_root'];?>/uploadHandler.php?stuff_id=<?=intval($_GET['stuff_id']);?>', // server side handler
//       name: '<?=$config['html_root'];?>/uploadfile.php', // upload parameter name        
//       progressUrl: '<?=$config['html_root'];?>/uploadProgress.php', // enables cross-browser progress support (more info below)
//       responseType: 'json',
//       allowedExtensions: ['jpg'],
//       maxSize: 1024, // kilobytes
//       hoverClass: 'ui-state-hover',
//       focusClass: 'ui-state-focus',
//       disabledClass: 'ui-state-disabled',
//       onSubmit: function(filename, extension) {
//           this.setFileSizeBox(sizeBox); // designate this element as file size container
//           this.setProgressBar(progress); // designate as progress bar
//         },         
//       onComplete: function(filename, response) {
//           if (!response) {
//             alert(filename + ' ошибка загрузки');
//             return false;            
//           }
//           else
//           {
//             $('#title-img').attr("src","<?=$config['html_root'];?>/img_uploads/0.jpg");
//             $('#title-img').attr("src","<?=$config['html_root'];?>/img_uploads/"+<?=intval($_GET['stuff_id']);?>+".jpg?s="+Math.random());
//           }
//           // do something with response...
//         }
// });   

function sendform()
{
  var data = CKEDITOR.instances.insideeditor.getData();
  $('#stuff_content').val(data);
  var data2 = CKEDITOR.instances.titleeditor.getData();
  $('#stuff_title').val(data2);
  var jqxhr = $.post( "", $( "#inside-form" ).serialize(), function() {
  alert( "Страница сохранена" );
}).fail(function() {
    alert( "Ошибка сохранения" );
  });
}
$( "#submit-inside" ).click(function() {
  sendform();
});

});

            </script>
</body>
</html>


<?
*/

?>